const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */


var root = '../../../';






elixir((mix) => {
    mix.styles([
        'shop-navbar.css',
        'simple-sidebar.css',
        // '../plugins/bzoom/css/style.css',
        '../plugins/select2-4.0.3/dist/css/select2.css',
        '../plugins/select2-4.0.3/dist/css/select2-bootstrap.css',
        '../plugins/nprogress/nprogress.css',

        'review.css',
        root+'node_modules/froala-editor/css/froala_editor.css',
        'star-rating.css',
        root+'vendor/kartik-v/bootstrap-star-rating/themes/krajee-svg/theme.css',

        root+'node_modules/ez-plus/css/jquery.ez-plus.css',

        // root+'node_modules/quill/dist/quill.snow.css',
        // root+'node_modules/quill/dist/quill.bubble.css',
        // root+'node_modules/quill/dist/quill.core.css',


        ],  'public/css/libs.css')

       .sass([
         root+'node_modules/bootstrap-sass/assets/stylesheets/bootstrap',
         root+'node_modules/select2/src/scss/',

         root+'vendor/components/font-awesome/scss/font-awesome.scss',
         root+'vendor/kartik-v/bootstrap-fileinput/sass/fileinput.scss',

         root+'node_modules/jquery-fancybox/source/scss/jquery.fancybox.scss',


       ],'public/scss/app.scss')

       .scripts([
         root+'vendor/kartik-v/bootstrap-fileinput/js/fileinput.js',
        //  root+'node_modules/quill/dist/quill.core.js',
        //  root+'node_modules/quill/dist/quill.js',
        //  root+'node_modules/quill/dist/quill.min.js',
         root+'node_modules/froala-editor/js/froala_editor.min.js',
         root+'node_modules/froala-editor/js/plugins/image.min.js',
         root+'node_modules/froala-editor/js/plugins/font_size.min.js',
         root+'node_modules/froala-editor/js/plugins/video.min.js',
         root+'node_modules/froala-editor/js/plugins/align.min.js',
         root+'node_modules/froala-editor/js/plugins/paragraph_format.min.js',
         root+'node_modules/froala-editor/js/plugins/paragraph_style.min.js',
         root+'node_modules/froala-editor/js/plugins/table.min.js',
         root+'node_modules/froala-editor/js/plugins/lists.min.js',
         root+'node_modules/froala-editor/js/plugins/code_view.min.js',
         root+'node_modules/froala-editor/js/plugins/draggable.min.js',
         root+'node_modules/froala-editor/js/plugins/char_counter.min.js',

         root+'node_modules/ez-plus/src/jquery.ez-plus.js',

         root+'vendor/kartik-v/bootstrap-star-rating/js/star-rating.js',
         root+'vendor/kartik-v/bootstrap-star-rating/themes/krajee-svg/theme.js',

        //  root+'node_modules/select2/src/js/jquery.select2.js',
        '../plugins/select2-4.0.3/dist/js/select2.full.js',
        '../plugins/nprogress/nprogress.js',
        // '../plugins/bzoom/js/jqzoom.js',

         'review.js',
       ],'public/js/lib.js')

       .webpack('app.js');

       mix.copy([
         'resources/assets/css/assets/',
       ],'public/assets/');

       mix.copy([
         'vendor/components/jquery/jquery.js',
       ],'public/js/');

       mix.copy([
         'node_modules/bootstrap-sass/assets/fonts/bootstrap/',
         'vendor/components/font-awesome/fonts/',

     ],'public/fonts/');

    mix.styles([
        'shop-navbar.css',
        '../frontpage/css/bootstrap.min.css',
        '../frontpage/css/font-awesome.min.css',
        '../frontpage/css/animate.min.css',
        '../frontpage/css/footer/footer.css',
        '../frontpage/css/responsive_bootstrap_carousel_mega.css',
    ],'public/css/frontpage.css');

    mix.scripts([
        '../frontpage/js/jquery-1.12.4.min.js',
        '../frontpage/js/bootstrap.min.js',
        '../frontpage/js/jquery.touchSwipe.min.js',
        '../frontpage/js/responsive_bootstrap_carousel.js',
    ],'public/js/frontpage.js');


});


// elixir((mix) => {
//     mix.styles([
//             '../dailyshop/css/nouislider.css',
//             '../dailyshop/css/jquery.smartmenus.bootstrap.css',
//             '../dailyshop/css/jquery.simpleLens.css',
//             '../dailyshop/css/slick.css',
//         ],  'public/css/libs.css')
//         .scripts([
//           '../dailyshop/js/custom.js',
//           '../dailyshop/js/jquery.simpleGallery.js',
//           '../dailyshop/js/jquery.simpleLens.js',
//           '../dailyshop/js/jquery.smartmenus.js',
//           '../dailyshop/js/jquery.smartmenus.bootstrap.js',
//           '../dailyshop/js/nouislider.js',
//           '../dailyshop/js/slick.js',
//        ])
//        .sass('app.scss')
//        .webpack('app.js');
// });
