<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $review_status = new App\ReviewStatus;
        $review_status->name = 'draft';
        $review_status->save();

        $review_status = new App\ReviewStatus;
        $review_status->name = 'pending';
        $review_status->save();

        $review_status = new App\ReviewStatus;
        $review_status->name = 'approved';
        $review_status->save();

        $review_status = new App\ReviewStatus;
        $review_status->name = 'rejected';
        $review_status->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_statuses');
    }
}
