<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->string('value')->default(0);
            $table->timestamps();
        });

        $point_settings = new App\PointSetting;
        $point_settings->key = 'BASE_POINTS_PER_REVIEW';
        $point_settings->save();

        $point_settings = new App\PointSetting;
        $point_settings->key = 'POINTS_TO_XP_CONVERSION_ALGORITHM';
        $point_settings->save();

        $point_settings = new App\PointSetting;
        $point_settings->key = 'BASE_POINTS_FOR_EQUAL_RANK_CHANGE';
        $point_settings->save();

        $point_settings = new App\PointSetting;
        $point_settings->key = 'POINTS_TO_RANK_CONVESTION_ALGORITHM';
        $point_settings->save();

        $point_settings = new App\PointSetting;
        $point_settings->key = 'ENABLE_MANUAL_POINT_RANK_SYSTEM';
        $point_settings->value = '1';
        $point_settings->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_settings');
    }
}
