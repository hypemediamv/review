<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active');
            $table->timestamps();
        });

        $user_group = new App\UserGroup;
        $user_group->name = 'Administrator';
        $user_group->is_active = true;
        $user_group->save();
        $user_group = new App\UserGroup;
        $user_group->name = 'Moderator';
        $user_group->is_active = true;
        $user_group->save();
        $user_group = new App\UserGroup;
        $user_group->name = 'Data Input';
        $user_group->is_active = true;
        $user_group->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_groups');
    }
}
