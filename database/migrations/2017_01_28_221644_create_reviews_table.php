<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->string('title');
            $table->longText('review');
            $table->float('rating')->default(0);
            $table->float('review_score')->default(0);
            $table->integer('created_by');
            $table->integer('moderated_by')->nullable();
            $table->integer('status_id')->default(1);
            $table->integer('user_type_id')->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
