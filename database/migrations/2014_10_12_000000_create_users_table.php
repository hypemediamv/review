<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('user_group_id');
            $table->boolean('is_expert')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        $user = new App\User;
        $user->name = 'shimax';
        $user->email = 'spyhacker@gmail.com';
        $user->password = bcrypt('123456');
        $user->user_group_id = 1;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
