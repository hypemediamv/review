<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

  public function item(){
    return $this->belongsTo(Item::class,'item_id');
  }

  public function user(){
    return $this->belongsTo(User::class,'created_by');
  }

  public function scopeGetUserReviews($query, $user_id){
    return $query->where('created_by','=',$user_id)->with('item.item_images.image','status','user','expert_custom_ratings.custom_rating')->get();
  }

  public function status(){
    return $this->belongsTo(ReviewStatus::class,'status_id');
  }

  public function expert_custom_ratings(){
    return $this->hasMany(ExpertCustomRating::class, 'review_id');
  }
    //
}
