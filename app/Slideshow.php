<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{
    protected $fillable = ['name', 'is_active', 'slideshow_position_id'];

    public function slideShowPosition(){
        return $this->belongsTo(SlideshowPosition::class, 'slideshow_position_id');
    }

    public function scopeGetTopSlider($query){
      return $query->where('slideshow_position_id', 1)->where('is_active',1)->first();
    }

    public function slides(){
      return $this->hasMany(Slide::class, 'slideshow_id');
    }


}
