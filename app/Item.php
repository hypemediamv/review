<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

  public function item_images(){
    return $this->hasMany(ItemImage::class, 'item_id');
  }

  public function user(){
    return $this->belongsTo(User::class,'created_by');
  }

  public function category(){
    return $this->belongsTo(Category::class,'category_id');
  }

  public function reviews(){
    return $this->hasMany(Review::class, 'item_id');
  }
    //
}
