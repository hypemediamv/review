<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleItem extends Model
{

  public function item(){
    return $this->belongsTo(Item::class, 'item_id');
  }

  public function module(){
    return $this->belongsTo(Module::class, 'module_id');
  }
    //
}
