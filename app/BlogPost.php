<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{

  public function blog_category(){
    return $this->belongsTo(BlogCategory::class, 'blog_category_id');
  }

  public function scopeWhereBlogCategory($query, $blog_category_id){
    return $query->where('blog_category_id',$blog_category_id);
  }
    //
}
