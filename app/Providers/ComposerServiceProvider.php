<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontpage.partials.header', \App\Http\ViewComposers\HeaderComposer::class);
        // view()->composer('frontpage.partials.module', \App\Http\ViewComposers\ModuleComposer::class);
        view()->composer('frontpage.partials.top-slider', \App\Http\ViewComposers\TopSliderComposer::class);
        view()->composer('frontpage.index', \App\Http\ViewComposers\ModuleComposer::class);
      }
    public function register()
    {
        //
    }
}
