<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
  public function module_position(){
    return $this->belongsTO(ModulePosition::class, 'module_position_id');
  }

  public function module_category(){
    return $this->hasOne(ModuleCategory::class, 'module_id');
  }

  public function module_reviews(){
    return $this->hasMany(ModuleReview::class, 'module_id');
  }

  public function module_items(){
    return $this->hasMany(ModuleItem::class, 'module_id');
  }
    //
}
