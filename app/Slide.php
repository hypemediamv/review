<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['title', 'name', 'description', 'slideshow_id', 'image_id', 'created_by'];

    public function slideshow() {
        return $this->belongsTo(Slideshow::class, 'slideshow_id');
    }

    public function image() {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
