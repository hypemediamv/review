<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemImage extends Model
{
  public static function boot()
  {
      parent::boot();

      // cause a delete of a product to cascade to children so they are also deleted
      static::deleting(function($item_image)
      {
          $item_image->image()->delete();
      });
  }

  public function item(){
    return $this->belongsTo(Item::class,'item_id');
  }

  public function image(){
    return $this->belongsTo(Image::class,'image_id');
  }




    //
}
