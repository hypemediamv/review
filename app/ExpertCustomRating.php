<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpertCustomRating extends Model
{

  public function custom_rating(){
    return $this->belongsTo(CustomRating::class, 'custom_rating_id');
  }

  public function review(){
    return $this->belongsTo(Review::class, 'review_id');
  }
    //
}
