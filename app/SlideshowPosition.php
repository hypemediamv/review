<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideshowPosition extends Model
{
    protected $fillable = ['name'];


    public function slideshow(){
      return $this->hasOne(Slideshow::class, 'slideshow_position_id');
    }


}
