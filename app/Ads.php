<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $fillable = ['name', 'image_id', 'owner'];

    public function image() {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
