<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class HeaderComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user_count = \App\User::all();
        $view->with('user_count', $user_count->count());
    }
}