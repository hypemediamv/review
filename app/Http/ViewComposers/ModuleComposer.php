<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class ModuleComposer
{

    private function getModuleByPosition($position){

      $module = \App\Module::whereHas('module_position', function($query) use ($position){
        return $query->where('name',$position);
      })->first();

      if($module!=null){
        $module->load('module_items.item.item_images.image', 'module_category.category.items.item_images.image');
      }

      return $module;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void'     */
    public function compose(View $view)
    {

        // dd($front_page_module, $front_page_module_2);




        $view->with([
          'front_page_module' =>$this->getModuleByPosition('FRONTPAGE_MODULE_1'),
          'front_page_module_2'=> $this->getModuleByPosition('FRONTPAGE_MODULE_2')
        ]);
    }
}
