<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Slideshow;

class TopSliderComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $top_slider = Slideshow::GetTopSlider()->first();

        if($top_slider != null){
          $top_slider->load('slides.image');

        }
        // dd($top_slider);

       $view->with('top_slider', $top_slider);
    }
}
