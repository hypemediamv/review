<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Http\Requests\AdsRequest;
use App\Image;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getAll()
    {
        $ads = Ads::with('image')->get();
        return $ads;
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getRecord($id)
    {
        $ads = Ads::findorFail($id);
        return $ads;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = $this->getAll();

        return view('dashboard.ads.index')->with(['ads' => $ads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdsRequest $request)
    {
        dd($request->all());
        if($request->user() != null){
            $name_check = Ads::where('name',$request->name)->exists();

            if($name_check){
                return redirect()->back()->with(['message' => 'Ads already exists', 'type' => 'warning']);
            }

            $changed_path = $this->getImagePath($request);
            
            $image = new Image();
            $image->filename = $request->image->getClientOriginalName();
            $image->path =$changed_path;
            $image->save();

            $ads = new Ads();
            $ads->name = $request->name;
            $ads->owner = $request->owner;
            $ads->image()->associate($image);
            $ads->expiry_date = $request->expiry_date;
            $ads->created_by = $request->user()->id;
            $ads->save();

            return redirect()->back()->with(['message' => $ads->name.' has been successfully created', 'type' => 'success']);

        }else{
            return redirect()->back()->with(['message' => 'Please login to add Ads', 'type' => 'danger']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ads = $this->getRecord($id)->with('user','image')->firstorFail();
        return view('dashboard.ads.partials.form',[ 'ads' => $ads ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ads = $this->getRecord($id)->with('user','image')->firstorFail();

        if ($request->image) {

            //getting path for for new image
            $changed_path = $this->getImagePath($request);

            //update ads and image file
            $this->updateAdFields($request, $ads);

            //delete existing image from file
            $file = $ads->image->path;
            $filename = storage_path().'/app/public'.$file;
            \File::delete($filename);

            $ads->image->filename = $request->image->getClientOriginalName();
            $ads->image->path =$changed_path;
            $ads->image->save();
            $ads->save();
        } else {
            $this->updateAdFields($request, $ads);
            $ads->save();
        }

        return redirect()->back()->with(['message' => $ads->name.' has been successfully updated', 'type' => 'success']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ads = $this->getRecord($id);
        $image = Image::where('id', $ads->image_id)->get();
        $image[0]->delete();
        $file= $image[0]['path'];
        $filename = storage_path().'/app/public'.$file;
        \File::delete($filename);;

        $ads->delete();

        return redirect()->back()->with(['message' => 'Ads has been successfully deleted', 'type' => 'success']);
    }

    /**
     * @param Request $request
     * @param $ads
     */
    private function updateAdFields(Request $request, $ads)
    {
        $ads->name = $request->name;
        $ads->owner = $request->owner;
        $ads->expiry_date = $request->expiry_date;
        $ads->created_by = $request->user()->id;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function getImagePath(Request $request)
    {
        $path = $request->image->store('public/images');
        $changed_path = str_replace("public/", "/", $path);
        return $changed_path;
    }

}
