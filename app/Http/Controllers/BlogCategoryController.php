<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use App\BlogPost;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    private function getBlogCategory($id){
      return BlogCategory::find($id);
    }

    private function countWhereBlogCategory($blog_category_id){
      return BlogPost::WhereBlogCategory($blog_category_id)->count();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $blog_category = new BlogCategory;
      $blog_category->name = $request->name;
      $blog_category->save();

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $blog_category = $this->getBlogCategory($id);
      $blog_category->name = $request->name;
      $blog_category->update();

      return redirect()->back();
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


      $blog_category = $this->getBlogCategory($id);

      $blog_post_count = $this->countWhereBlogCategory($blog_category->id);
      if($blog_post_count!=0){
        return redirect()->back()->with(['message' => 'A category wtih posts cannot be deleted', 'type'=>'warning']);

      }else{
        $blog_category->delete();

      }

      return redirect()->back();
        //
    }
}
