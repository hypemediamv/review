<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\BlogPost;
use App\BlogCategory;


class BlogPostController extends Controller
{
    private function getAuthenticatedUserId(){
      return Auth::user()->id;
    }

    private function getBlogCategory($id){
      return BlogCategory::find($id);
    }

    private function getBlogPost($id){
      return BlogPost::with('blog_category')->find($id);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $blog_post = new BlogPost;
      $blog_post->name = $request->name;
      $blog_post->content = $request->content;
      if($this->getAuthenticatedUserId()!= null){
        $blog_post->created_by = $this->getAuthenticatedUserId();

      }else{
        $blog_post->created_by = 1;
      }
      $blog_post->blog_category()->associate($this->getBlogCategory($request->blog_category_id));
      $blog_post->save();

      return redirect()->back()->with(['type' => 'success' , 'message' => 'Blog Post '.$blog_post->name.'has been added successfully']);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $blog_post = $this->getBlogPost($id);
      return redirect()->back()->with(['view_post' => $blog_post]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $blog_post = $this->getBlogPost($id);
      return redirect()->back()->with(['edit_post' => $blog_post]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $blog_post =  $this->getBlogPost($id);
      $blog_post->name = $request->name;
      $blog_post->content = $request->content;
      if($this->getAuthenticatedUserId()!= null){
        $blog_post->created_by = $this->getAuthenticatedUserId();

      }else{
        $blog_post->created_by = 1;
      }
      $blog_post->blog_category()->associate($this->getBlogCategory($request->blog_category_id));
      $blog_post->update();

      return redirect()->back()->with(['edit_post' => $blog_post, 'type' => 'success', 'message' => 'Blog Post has been updated successfully']);

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $blog_post = $this->getBlogPost($id);
      $blog_post->delete();

      return redirect()->back()->with(['type' => 'success', 'message' => ' Blog  Post has been deleted successfully']);
        //
    }
}
