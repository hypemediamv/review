<?php

namespace App\Http\Controllers;
use App\Module;
use App\ModulePosition;
use App\Category;
use App\ModuleCategory;
use App\ModuleItem;
use App\Item;
use Illuminate\Http\Request;

class ModuleController extends Controller
{

    private function getAllModules(){
      return Module::with('module_position','module_category.category','module_items.item','module_reviews')->get();
    }

    private function pluckAllItems(){
      return Item::all()->pluck('name','id');
    }
    private function getAllModulePositions(){
      return ModulePosition::all();
    }

    private function getItem($id){
      return Item::find($id);
    }

    private function getAllReviewCategories(){
      return Category::all();
    }

    private function getModule($id){
      return Module::find($id);
    }

    private function getModulePosition($id){
      return ModulePosition::find($id);
    }

    private function getCategory($id){
      return Category::find($id);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


      return view('dashboard.modules.index',[
        'modules' => $this->getAllModules() ,
        'module_positions' => $this->getAllModulePositions(),
        'review_categories'=> $this->getAllReviewCategories(),
        'items' => $this->pluckAllItems()
      ]);

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $by_category = false;
      $by_items = false;

      switch($request->module_type){
        case 'category' : $by_category = true; break;
        case 'item' : $by_items = true; break;
        default : break;
      }



      $module_position = $this->getModulePosition($request->module_position_id);
      $category = $this->getCategory($request->review_category_id);

      try{
        $module = new Module;
        $module->name = $request->name;
        $module->description = $request->description;
        $module->limit = $request->limit;
        $module->by_category = $by_category;
        $module->by_items = $by_items;
        $module->link = $request->link;
        $module->module_position()->associate($module_position);
        $module->save();

      }catch(\Exception $e){

        return redirect()->back()->with(['type' => 'warning', 'message' => 'Module Position is already occupied']);
      }


      if($by_category == true){
        $module_category = new ModuleCategory;
        $module_category->category()->associate($category);
        $module_category->module()->associate($module);
        $module_category->save();
      }

      if($by_items == true){
        if($request->item_list!= null){


          foreach($request->item_list as $item_id){


            $item = $this->getItem($item_id);

            $module_item = new ModuleItem;
            $module_item->item()->associate($item);
            $module_item->module()->associate($module);
            $module_item->save();
          }
        }
      }





      return redirect()->back()->with(['type' => 'success' , 'message' => 'Module added successfully']);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $module = $this->getModule($id);
      $module->delete();
      return redirect()->back()->with(['type'=> 'success', 'message' => 'Module has been deleted successfully']);
        //
    }
}
