<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomRating;

class CustomRatingController extends Controller
{

    private function messageBox($type, $title){
      $message = new \StdClass;
      $message->title = $title;
      $message->type = $type;
      return $message;

    }

    private function getAllCustomRatings(){
      return CustomRating::get();
    }

    private function getCustomRatingById($id){
      return CustomRating::find($id);
    }

    private function getCustomRatingsByIdArray($id_array){
      return CustomRating::whereIn('id',$id_array)->get();
    }

    private function getCustomRatingByName($name){
      return CustomRating::where('name',$name)->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $custom_ratings = CustomRating::get();
      return view('dashboard.custom_rating.index',['custom_ratings' => $custom_ratings]);
        //
    }


    public function expert_custom_rating_session(Request $request){
      return redirect()->back()->withInput()->with([
        'custom_ratings' =>  $this->getCustomRatingsByIdArray($request->custom_rating_list),
        'type'=> 'success',
        'message' => 'You have successfully added custom ratings to rate'
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if($this->getCustomRatingByName($request->name) != '[]'){
        $message_box = $this->messageBox('warning',$request->name.' already exists, please try another name');

      }else{
        $custom_rating = new CustomRating;
        $custom_rating->name = $request->name;
        $custom_rating->save();

        $message_box = $this->messageBox('success',  $custom_rating->name.'Custom Rating added successfully');
      }

      return redirect()->back()->with(['message' =>$message_box->title, 'type' => $message_box->type]);




      // return redirect()->back()->with(['messages'=> $message_box->title, 'type'=> $message_box->type]);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $custom_rating = CustomRating::find($id);
      $custom_rating_name = $custom_rating->name;
      $custom_rating->delete();

      return redirect()->back()->with(['message_type' => 'successs', 'message_title' =>  $custom_rating_name.'Custom Rating has been deleted successfully']);

        //
    }
}
