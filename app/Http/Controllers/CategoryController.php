<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CategoryRequest;

use App\Category;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $categories = Category::with('parent','user')->withDepth()->get();


      $category_tree = Category::get()->toTree();


      // dd($categories);
      $parent_categories = $categories->pluck('name','id');
      $parent_categories->prepend('No Parent' , '0');



      return view('dashboard.category.index',['categories' => $categories, 'parent_categories' => $parent_categories, 'category_tree' => $category_tree]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {




      if($request->user() != null){
        $name_check = Category::where('name',$request->name)->count();
        if($name_check != 0){
          return redirect()->back()->with(['message' => 'Category already exists', 'type' => 'warning']);
        }


        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->created_by = $request->user()->id;
        $category->save();

        if($request->parent > 0){

          $parent_category = Category::find($request->parent);
          $parent_category->appendNode($category);

        }


        return redirect()->back()->with(['message' => 'Category '.$category->name.' has been created successfully', 'type' => 'success']);

      }else{

        return redirect()->back()->with(['message' => 'Please login to add categories', 'type' => 'danger']);



      }



        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {



      $category = Category::find($id);
      return redirect()->back()->with(['edit_category' => $category]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
      // Category::rebuild();
      $category = Category::find($id);
      $category->name = $request->name;
      $category->description = $request->description;
      $category->created_by = $request->user()->id;
      $category->update();
      // $category->makeRoot();


      if($request->parent != 0){
        $parent_category = Category::find($request->parent);
        $parent_category->appendNode($category);
      }else{
        $category->saveAsRoot();
      }



      // $category->name = $request->name;
      // $category->description = $request->description;
      // $category->created_by = $request->user()->id;
      // $category->
      // $category->update();
    //




      return redirect()->back()->with(['message' => 'Category '.$category->name.' has been updated successfully',  'type' => 'success' ,'edit_category' => $category]);

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $category = Category::find($id);

      $sub_categories = $category->getDescendants();
      if(count($sub_categories) != 0){
        return redirect()->back()->with([
          'message' => 'This '.$category->name.' category has the following sub-categories, A category must not have any sub-categories for deletion',
           'type' => 'warning',
           'list' => $sub_categories
         ]);

      }
        $category->delete();
        return redirect()->back()->with(['message' => 'Category '.$category->name.' has been deleted successfully',  'type' => 'success']);
    }
}
