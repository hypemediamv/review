<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;
use App\Category;
use App\Image;
use App\ItemImage;
use Auth;


class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     private function getAllCategories(){
         return  Category::with('children')->get();;
     }

     private function getAllItems(){
       return  Item::with('user','item_images.image')->get();
     }

     private function getItem($id){
       return Item::with('user','item_images.image')->find($id);
     }

     private function saveItemImages($item, $request){

       foreach($request->item_images as $uploaded_product_image){

         $path = $uploaded_product_image->store('public/images/');
         $changed_path = str_replace("public/","/",$path);
         $image = new Image;
         $image->filename = $uploaded_product_image->getClientOriginalName();
         $image->path =$changed_path;
         $image->save();

         $item_image = new ItemImage;
         $item_image->item()->associate($item);
         $item_image->image()->associate($image);
         $item_image->created_by = Auth::user()->id;
         $item_image->save();

       }

     }

    public function index()
    {

      $items =$this->getAllItems();
      $categories = $this->getAllCategories();


      if(!$categories->isEmpty()){
        return view('dashboard.item.index',['items' => $items, 'categories' => $categories]);
      }else{
        return redirect()->action('CategoryController@index')->with(['message' => ' An Item cannot be added without a category, please add categories to add items', 'type'=>'warning']);
      }





        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




      if($request->user() != null){
        $name_check = Item::where('name',$request->name)->count();

        if($name_check != 0){
          return redirect()->back()->with(['message' => 'Item already exists', 'type' => 'warning']);
        }



        $item = new Item;
        $item->name = $request->name;

        $item->description = $request->description;
        $item->created_by = $request->user()->id;
        $item->category_id = $request->category_id;
        $item->save();

        $this->saveItemImages($item, $request);



        return redirect()->back()->with(['message' => $item->name.' has been successfully created', 'type' => 'success']);

      }else{
        return redirect()->back()->with(['message' => 'Please login to add items', 'type' => 'danger']);

      }



        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      return view('dashboard.item.show',[ 'item' => $this->getItem($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      return view('dashboard.item.partials.edit',[ 'item' => $this->getItem($id), 'categories' => $this->getAllCategories()]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


      $item = $this->getItem($id);
      $item->name = $request->name;
      $item->description = $request->description;
      $item->created_by = $request->user()->id;
      $item->update();

      $image_count = count($request->item_images);

    //
      if( $image_count > 0 && $request->item_images[0] != ''){

          if($image_count == 1){
            $image_message = $image_count.' image';
          }else{
            $image_message = $image_count.' images';
          }

          $this->saveItemImages($item, $request);


        return redirect()->back()->with(['message' => $item->name.' has been successfully updated with '.$image_message , 'type' => 'success']);
      }else{
        return redirect()->back()->with(['message' => $item->name.' has been successfully updated', 'type' => 'success']);
      }


        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $item = $this->getItem($id);
      $item->delete();

      return redirect()->back()->with(['message' => 'Item has been successfully deleted', 'type' => 'success']);
        //-
    }
}
