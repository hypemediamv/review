<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Item;
use App\ReviewStatus;
use App\UserType;
use App\CustomRating;
use App\ExpertCustomRating;
use Session;
use Auth;

class ReviewController extends Controller
{

    public function __construct(){
      $this->review_statuses = (object) ReviewStatus::get()->pluck('id','name')->toArray();
      $this->user_types = (object) UserType::get()->pluck('id','name')->toArray();

    }

    private function getCustomRatingById($id){
      return CustomRating::find($id);
    }

    private function getCustomRatingsByIdArray($id_array){
      return CustomRating::whereIn('id',$id_array)->get();
    }
    private function countReviewWithExpertCustomRating($review_id, $custom_rating_id){
      return ExpertCustomRating::where('custom_rating_id', $custom_rating_id)->where('review_id', $review_id)->count();
    }

    private function getExpertCustomRating($review_id, $custom_rating_id){
      return ExpertCustomRating::where('review_id', $review_id)->where('custom_rating_id', $custom_rating_id)->first();
    }




    private function getReview($id){
        return  Review::with('item.item_images.image','expert_custom_ratings.custom_rating')->findorFail($id);
    }

    private function getAllReviews(){
        return Review::with('status','item','user')->get();
    }

    private function getUserReviews($user_id){
        return Review::GetUserReviews($user_id);
    }

    private function getItem($id)  {
        return  Item::findorFail($id);
    }

    private function getAllItems(){
      return Item::with('user','item_images.image')->get();
    }

    private function getAllCustomRatings(){
      return CustomRating::get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



      $reviews = $this->GetUserReviews(Auth::user()->id);
      $items = $this->getAllItems();
      $custom_ratings = $this->getAllCustomRatings();



      if($items!= '[]'){
        return view('dashboard.review.index',[ 'reviews' => $reviews, 'items' => $items, 'custom_ratings' => $custom_ratings]);


      }else{
        return redirect()->action('ItemController@index')->with(['message' => ' A review cannot be added without an item, please add items to review', 'type'=>'warning']);

      }

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if(isset($request->add_custom_rating)){

        return redirect()->back()->withInput()->with([
          'selected_custom_ratings' =>  $this->getCustomRatingsByIdArray($request->custom_rating_list),
          'type'=> 'success',
          'message' => 'You have successfully added custom ratings to rate'
        ]);
      }



      $item = $this->getItem($request->item_id);

      $user_review_count = Review::where('created_by', $request->user()->id)->where('item_id', $item->id)->get()->count();

      // if( $user_review_count != '0'){
      //   return redirect()->back()->with(['message' => 'You have already reviewed <strong>'.$item->name.'</strong>. You cannot review the same product twice', 'type' => 'warning']);
      // }

      $review = new Review;
      $review->title = $request->title;
      $review->review = $request->review;
      $review->rating = $request->rating;
      $review->created_by = $request->user()->id;
      $review->user_type_id = $this->user_types->regular;

      if(isset($request->publish)){
        $review->status_id = $this->review_statuses->pending;
        $message = 'Your review for'.'<strong>'.$item->name.'</strong> has been successfully published for moderation';
      }else{
        $review->status_id = $this->review_statuses->draft;
        $message = 'Your review for'.'<strong>'.$item->name.'</strong> has been saved to drafts';

      }
      $review->item()->associate($item);
      $review->save();

      if(isset($request->selected_custom_ratings) && Auth::user()->is_expert == 1){

        foreach($request->selected_custom_ratings as $custom_rating_id => $custom_rating_value){

          $custom_rating = $this->getCustomRatingById($custom_rating_id);

          $expert_custom_rating = new ExpertCustomRating;
          $expert_custom_rating->review()->associate($review);
          $expert_custom_rating->custom_rating()->associate($custom_rating);
          $expert_custom_rating->rating = $custom_rating_value;
          $expert_custom_rating->save();

        }
      }




      return redirect()->back()->with(['message' => $message, 'type' => 'success']);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $review = $this->getReview($id);
      return view('dashboard.review.show',['review'=>$review]);

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $review = $this->getReview($id);

      if($review->status_id != $this->review_statuses->draft){
        Session::flash('message' , 'Your Review  for '.'<strong>'.$this->getItem($review->item_id)->name.'</strong> has been published for Moderation. Editing is disabled at this stage.');
        Session::flash('type' , 'warning');
      }
      //get the custom_rating from expert_custom_rating to populate select field
      $selected_custom_ratings = [];
      foreach($review->expert_custom_ratings as $expert_custom_rating){
        $selected_custom_ratings[] = $expert_custom_rating->custom_rating->id;
      }




      return view('dashboard.review.edit',[
        'review'=>$review ,
         'items'=> $this->getAllItems(),
         'custom_ratings' => $this->getAllCustomRatings(),
         'selected_custom_ratings' => $selected_custom_ratings

       ]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $review = $this->getReview($id);
      $pre_selected_custom_ratings = $request->selected_custom_ratings;
      $newly_selected_custom_ratings = $request->custom_rating_list;

      if(isset($request->add_custom_rating)){
        //if any rating is added, add those ratings

        if($newly_selected_custom_ratings!= null){
          foreach($newly_selected_custom_ratings as $newly_selected_custom_rating_id){
            $custom_rating = $this->getCustomRatingById($newly_selected_custom_rating_id);

            if($this->countReviewWithExpertCustomRating($review->id, $custom_rating->id) == 0){
              $expert_custom_rating = new ExpertCustomRating;
              $expert_custom_rating->review()->associate($review);
              $expert_custom_rating->custom_rating()->associate($custom_rating);
              $expert_custom_rating->rating = 5;
              $expert_custom_rating->save();
            }
          }
        }

        if($pre_selected_custom_ratings != null){
          // check if any preselected custom rating is unrated, if unrated, delete those custom_ratings
          foreach($pre_selected_custom_ratings as $custom_rating_id => $custom_rating_value){
            if($custom_rating_value == 0){
              $zeroed_expert_custom_rating = $this->getExpertCustomRating($review->id, $custom_rating_id);
              $zeroed_expert_custom_rating->delete();
            }
          }
        }
        return redirect()->back();
      }

      if($pre_selected_custom_ratings!= null){

        foreach($pre_selected_custom_ratings as $custom_rating_id => $custom_rating_value){
          $update_expert_custom_rating = $this->getExpertCustomRating($review->id, $custom_rating_id);
          $update_expert_custom_rating->rating = $custom_rating_value;
          $update_expert_custom_rating->update();
        }
      }


      $item = $this->getItem($review->item_id);
      $review->title = $request->title;
      $review->review = $request->review;
      $review->rating = $request->rating;

      if(!isset($request->draft)){
        $review->status_id = $this->review_statuses->pending;
        $message = 'Your review for <strong> '.$item->name.'  </strong> has been succesfully published for moderation';
      }else{
        $message = 'Your review for <strong> '.$item->name.' </strong> has been succesfully saved to draft';

      }

      $review->update();

      return redirect()->back()->with(['message' => $message , 'type' => 'success']);

        //
    }

    public function publish(Request $request, $id)
    {

       $review = $this->getReview($id);
       $review->status_id = $this->review_statuses->pending;
       $review->update();


       return back()->with(['message' => 'Your review for '.'<strong> '.$this->getItem($review->item_id)->name.'</strong> has been successfully created and is awaiting moderation', 'type' => 'success']);


    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
