<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class HomeController extends Controller
{

    private function getItemWithReviews($item_id){
      return Item::findOrfail($item_id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function item_index($id){

      $item = $this->getItemWithReviews($id);
      $item->load('reviews','item_images.image');


      return view('frontpage.items.item',['item' => $item]);
    }
}
