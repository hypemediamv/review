<?php

namespace App\Http\Controllers;

use App\Http\Requests\SlideRequest;
use App\Image;
use App\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::get();

        return view('dashboard.slideshows.index', compact('slides'));
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getRecord($id)
    {
        $slide = Slide::findorFail($id);
        return $slide;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \app\Http\Requests\SlideRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->user() != null){

            $changed_path = $this->getImagePath($request);

            $image = new Image();
            $image->filename = $request->image->getClientOriginalName();
            $image->path =$changed_path;
            $image->save();

            $slide = new Slide();
            $slide->title = $request->title;
            $slide->description = $request->description;
            $slide->slideshow_id = $request->slideshow_id;
            $slide->image()->associate($image);
            $slide->created_by = $request->user()->id;
            $slide->save();

            return redirect()->back()->with(['message' => $slide->title.' has been successfully created', 'type' => 'success']);

        }else{
            return redirect()->back()->with(['message' => 'Please login to add Ads', 'type' => 'danger']);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = $this->getRecord($id);
        $image = Image::where('id', $slide->image_id)->get();
        $image[0]->delete();
        $file= $image[0]['path'];
        $filename = storage_path().'/app/public'.$file;
        \File::delete($filename);;

        $slide->delete();

        return redirect()->back()->with(['message' => 'Slide has been successfully deleted', 'type' => 'success']);

    }


    /**
     * @param Request $request
     * @return mixed
     */
    private function getImagePath(Request $request)
    {
        $path = $request->image->store('public/images');
        $changed_path = str_replace("public/", "/", $path);
        return $changed_path;
    }
}
