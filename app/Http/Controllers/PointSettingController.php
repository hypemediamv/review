<?php

namespace App\Http\Controllers;

use App\PointSetting;
use App\PointLevel;

use Illuminate\Http\Request;

class PointSettingController extends Controller
{

    private function getAllPointSettings(){
      return PointSetting::all();
    }

    private function getAllPointLevels(){
      return PointLevel::all();
    }

    private function getKeyValue($key){
      return PointSetting::where('key', $key)->first();
    }

    private function getPointLevel($id){
      return PointLevel::find($id);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('dashboard.settings.points',['point_settings' =>  $this->getAllPointSettings() , 'point_levels' =>  $this->getAllPointLevels()]);

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      foreach($request->keys as $key => $value){

        $key_value = $this->getKeyValue($key);
        $key_value->value = $value;
        $key_value->update();

      }

      return redirect()->back();
    }

    public function store_level(Request $request){
      $point_level = new PointLevel;
      $point_level->rank = $request->rank;
      $point_level->points = $request->points;
      $point_level->base_point_increment_percentage = $request->base_point_increment_percentage;
      $point_level->save();

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroy_level(Request $request)
    {
      $point_level =$this->getPointLevel($request->id);
      $point_level->delete();

      return redirect()->back();
        //
    }
}
