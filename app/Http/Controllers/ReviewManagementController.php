<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\ReviewStatus;
use App\UserType;
use App\Item;

class ReviewManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){
       $this->review_statuses = (object) ReviewStatus::get()->pluck('id','name')->toArray();
       $this->user_types = (object) UserType::get()->pluck('id','name')->toArray();

     }

    private function getItem($id){
      return Item::find($id);
    }

    private function getReview($id){
      return Review::find($id);
    }

    private function getAllReviews(){
      return Review::with('item.item_images.image')->get();
    }

    private function updateReviewStatus($id, $status){
      $review = $this->getReview($id);
      $review->status_id = $status;
      $review->update();

      return $review;
    }

    private function updateItemAverageByReviewId($id){
      $review = $this->getReview($id);
      if($review->status_id != $this->review_statuses->approved){

          $item = $this->getItem($review->item_id);
          
          $total = $item->average_user_rating + $review->rating;
          $divisor = $item->user_count+1 * $total;
          $new_average = ($total/$divisor)*5;
          $rounded_average = floor($new_average * 2) / 2;

          $item->average_user_rating =   $rounded_average;
          $item->user_count = $item->user_count + 1;
          $item->update();

      }

    }


    public function index()
    {


      return view('dashboard.review.management.index',['reviews' => $this->getAllReviews()]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


      $message_begin = 'This review has been marked as ';
      $message_end = ' successfully';

      if(isset($request->approve)){
        $this->updateItemAverageByReviewId($id);
         $this->updateReviewStatus($id, $this->review_statuses->approved);
         $message_status = 'approved';
      }

      if(isset($request->draft)){
         $this->updateReviewStatus($id, $this->review_statuses->draft);
         $message_status = 'a draft';

      }
      if(isset($request->reject)){
         $this->updateReviewStatus($id, $this->review_statuses->rejected);
         $message_status = 'rejected';

      }
      if(isset($request->pending)){
         $this->updateReviewStatus($id, $this->review_statuses->pending);
         $message_status = 'pending';

      }


      return redirect()->back()->with(['message' => $message_begin.'<strong>'.$message_status.'</strong>'.$message_end , 'type' => 'success']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
