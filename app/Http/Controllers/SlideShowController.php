<?php

namespace App\Http\Controllers;

use App\Http\Requests\SlideshowsRequest;
use App\Slide;
use App\Slideshow;
use App\SlideshowPosition;
use Illuminate\Http\Request;

class SlideShowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slide_shows = Slideshow::with('slideShowPosition')->get();
        $slideshow_positions = SlideshowPosition::get()->pluck('name', 'id');

        $slides = Slide::with('slideshow', 'image')->get();

//        dd($slides);
        return view('dashboard.slideshows.index', compact('slide_shows','slideshow_positions', 'slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \app\Http\Requests\SlideshowsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SlideshowsRequest $request)
    {
        if ($request->is_active) {
            $active_slide_shows = Slideshow::all()->where('is_active', '=', 1);
            if ($active_slide_shows->count() > 0) {
                foreach ($active_slide_shows as $active_slide_show) {
                    $active = $active_slide_show->is_active = 0;
                    $active_slide_show->update([$active]);
                }
            }

        }

        Slideshow::create($request->all());
        return redirect()->back()->with(['message' => 'Slide Show created!', 'type' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Slideshow::with('slides')->where('id', $id)->firstOrFail();
        if ($data->slides != '[]') {
            return redirect()->back()->with(['message' => 'Delete all the belonging slides with the '.$data->name.' slide show!', 'type' => 'warning']);
        }
        $data->delete();
        return redirect()->back()->with(['message' => 'Slide Show deleted!', 'type' => 'success']);
    }
}
