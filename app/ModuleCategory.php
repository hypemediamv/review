<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleCategory extends Model
{

  public function category(){
    return $this->belongsTo(Category::class, 'category_id');
  }

  public function module(){
    return $this->belongsTo(Module::class, 'module_id');
  }
    //
}
