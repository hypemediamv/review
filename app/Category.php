<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Baum\Node;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
  use NodeTrait;

  protected $fillable = ['name','description','created_by'];

  public function user(){
    return $this->belongsTo(User::class, 'created_by');
  }

  public function items(){
    return $this->hasMany(Item::class, 'category_id');
  }
    //
}
