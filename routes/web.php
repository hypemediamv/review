<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontpage.index');
});

Route::get('/categories', function () {
    return view('frontpage.categories.index');
});

Route::get('/items/{item}','HomeController@item_index');

Auth::routes();


// Route::get('/home', 'HomeController@index');
Route::get('/admin', 'AdminController@index');


Route::group(['prefix' => 'admin'], function () {


  Route::group(['prefix' => 'settings'], function () {
    Route::resource('/points','PointSettingController');
    Route::post('/points/level','PointSettingController@store_level');
    Route::post('/points/level/destroy', 'PointSettingController@destroy_level');

  });

Route::resource('/slideshows','SlideshowController');
Route::resource('/slides','SlideController');
Route::resource('/modules','ModuleController');
Route::resource('/blog','BlogController');
Route::resource('/blog-post', 'BlogPostController');
Route::resource('/blog-category','BlogCategoryController');
Route::resource('/users', 'UserController');
Route::post('/users/update-user', 'UserController@update_user');
Route::resource('/items','ItemController');
Route::resource('/categories','CategoryController');
Route::resource('/images','ImageController');
Route::resource('/item-images','ItemImageController');
Route::resource('/ads','AdsController');
Route::patch('/review/publish/{review}','ReviewController@publish');

Route::post('/ecrs', 'CustomRatingController@expert_custom_rating_session');

Route::resource('/reviews','ReviewController');
Route::resource('/reviews-management','ReviewManagementController');
Route::resource('/custom-rating','CustomRatingController');
});
