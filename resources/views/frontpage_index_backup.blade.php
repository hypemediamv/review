<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review.mv</title>

    <!--======= front page css libs StyleSheet =========-->
    <link href="{{ asset('css/frontpage.css') }}" rel="stylesheet" media="all">

    <!-- front page custom styling -->
    <style>
        body {
            background: rgba(238, 238, 238, 1.0);
        }

        .red-box {
          margin-top:30%;
          text-align: center;
          margin-left: 15%;
          left:50%;

        }

        .red-margin-top{
            margin-top:15px;
        }
        /*.category-listing {*/
            /*padding: 10px;*/
            /*color: #000000;*/
            /*display: table;*/
            /*height: 450px;*/
            /*overflow: hidden;*/
            /*background: #FFFFFF;*/
         /*}*/

        /*.carousel-inner {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }*/


        .gp_products_inner {
            box-shadow: none;
            -webkit-box-shadow: none;
        }

        .gp_products_item_caption {
            text-align: center;
        }

        .gp_products_carousel_wrapper {
            background: #FFFFFF;
        }
        .category-listing hr {
            border-color: #e05050;
            border-width: 2pt;
        }

        #loginModal {
            color: #0a0a0a;

        }
    </style>

</head>

<body>

        <!-- Header -->
        @include('frontpage.partials.header')
        <!-- Page Content -->


            @include('frontpage.partials.top-slider')




        <div class="container-fluid">

          <div class="panel panel-default red-margin-top">
  <div class="panel-body">
          <div class="col-md-2">
            <div class="red-box">
              <h1>Featured Reviews </h1>
              <p> This could be another set of reviews bundled together in one neat package</p>
                <a href="{{ url('categories') }}" class="btn btn-danger">See more</a>


          </div>
        </div>
          <div class="col-md-10">

            <!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

            <div id="adv_gp_products_4_columns_carousel" class="carousel slide four_shows_one_move gp_products_carousel_wrapper" data-ride="carousel">


                <!--========= Wrapper for slides =========-->
                <div class="carousel-inner" role="listbox">

                    <!--========= 1st slide =========-->
                    <div class="item active">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?objects,camera" alt="gp product 001" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 1</a></li>
                                        <li><a href="#">Product 1 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--========= 2nd slide =========-->
                    <div class="item">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?objects,watch" alt="gp product 002" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 2</a></li>
                                        <li><a href="#">Product 2 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--========= 3rd slide =========-->
                    <div class="item">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?objects,books" alt="gp product 003" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 3</a></li>
                                        <li><a href="#">Product 3 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--========= 4th slide =========-->
                    <div class="item">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?objects,chair" alt="gp product 004" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 4</a></li>
                                        <li><a href="#">Product 4 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--========= 5th slide =========-->
                    <div class="item">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?nature,sea" alt="gp product 005" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 5</a></li>
                                        <li><a href="#">Product 5 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--========= 6th slide =========-->
                    <div class="item">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?nature,tree" alt="gp product 006" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 6</a></li>
                                        <li><a href="#">Product 6 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--========= 7th slide =========-->
                    <div class="item">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?objects,paper" alt="gp product 007" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 7</a></li>
                                        <li><a href="#">Product 7 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--========= 8th slide =========-->
                    <div class="item">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?objects,table" alt="gp product 008" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 8</a></li>
                                        <li><a href="#">Product 8 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!--======= Navigation Buttons =========-->

                <!--======= Left Button =========-->
                <a class="left carousel-control gp_products_carousel_control_left" href="#adv_gp_products_4_columns_carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left gp_products_carousel_control_icons" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <!--======= Right Button =========-->
                <a class="right carousel-control gp_products_carousel_control_right" href="#adv_gp_products_4_columns_carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right gp_products_carousel_control_icons" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->


          			</div>

          				<!--======= Navigation Buttons =========-->

          				<!--======= Left Button =========-->
          				<a class="left carousel-control portfolio_utube_carousel_control_left" href="#adv_portfolio_6_columns_utube_carousel" role="button" data-slide="prev">
          					<span class="fa fa-angle-left portfolio_utube_carousel_control_icons" aria-hidden="true"></span>
          					<span class="sr-only">Previous</span>
          				</a>

          				<!--======= Right Button =========-->
          				<a class="right carousel-control portfolio_utube_carousel_control_right" href="#adv_portfolio_6_columns_utube_carousel" role="button" data-slide="next">
          					<span class="fa fa-angle-right portfolio_utube_carousel_control_icons" aria-hidden="true"></span>
          					<span class="sr-only">Next</span>
          				</a>

          		</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->
              </div>
            </div>
          </div>
        </div>
        <!-- /#page-content-wrapper -->


<!--footer start from here-->

<div class="copyright">
  <div class="container">
    <div class="col-md-6">
      <p> Review.mv ©2016 - All Rights Reserved</p>
    </div>
    <div class="col-md-6">
      <ul class="bottom_ul">
        <li><a href="#">How it Works</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Contact us</a></li>
      </ul>
    </div>
  </div>
</div>

        <!--======= front page js libs =========-->
        <script src="{{ asset('js/frontpage.js') }}"></script>
        <script>
            $('#loginModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('Login')
            })
        </script>



</body>

</html>
