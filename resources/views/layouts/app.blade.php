<!DOCTYPE html>
<?php
ini_set('max_execution_time', 180); //3 minutes

 ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="{{ asset('css/frontpage.css') }}" rel="stylesheet" media="all">


    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <style>
        #login-header {
            background-color: #dc4c4c;
            height: 70px;
            padding-top: 10px;
            text-align: center;
            font-weight: bold;
            font-size: larger;
            margin-bottom: 50px;
        }
        #login-header a:link, #login-header a:visited {
            background-color: #dc4c4c;
            color: white;
            padding: 14px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
        }


        #login-header a:hover, #login-header a:active {
            background-color: #dc4c4c;
        }
        #login-header a {
            color: white;
            text-decoration: none;
        }

        .copyright {
            bottom: 0;
            position: fixed;
            width: 100%;

        }
    </style>
</head>
<body>
    <div id="app">
        @yield('default-navbar')
        <header>
            <div id="login-header">
                <a href="/" target="_parent">Review.mv</a>
            </div>
        </header>
        @yield('content')
    </div>

    <!--footer start from here-->
    <div class="copyright">
        <div class="container">
            <div class="col-md-6">
                <p> Review.mv ©2016 - All Rights Reserved</p>
            </div>
            <div class="col-md-6">
                <ul class="bottom_ul">
                    <li><a href="#">How it Works</a></li>
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contact us</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
