<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review.mv</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" media="all" >
    <link rel="stylesheet" href="{{ asset('css/libs.css') }}" media="all" >
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/dt-1.10.13/r-2.1.0/datatables.min.css"/>



    <script src="{{ asset('js/jquery.js') }}"></script>


    <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/dt-1.10.13/r-2.1.0/datatables.min.js"></script>
    <script src="{{ asset('js/lib.js') }}"></script>

    <script>
    // Animate loader off screen
</script>

    <script>



    $( document ).ready(function()
    {



      var $content = $(".review-content").hide();
      $(".review-toggler").on("click", function(e){
        $(".review-toggle").toggleClass("expanded");
        $content.slideToggle();
      });

      window.setTimeout(function() {
      $("#message-box").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
      });
      }, 1500);



      NProgress.start();
    NProgress.done();

    $('#red_rating').rating({
      hoverOnClear: false,
      containerClass: 'is-star',
      // step: 0,
      starCaptions: {0.5:'Extremely Poor', 1: 'Very Poor', 1.5:'Poor', 2: 'Below Average', 2.5: 'Average', 3: 'Above Average', 3.5:'Good', 4: 'Very Good', 4.5:'Excellent' ,5: 'Perfect'},
      // starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'}
  });

  $('.red_rating').rating({
    hoverOnClear: false,
    containerClass: 'is-star',
    // step: 0,
    starCaptions: {0.5:'Extremely Poor', 1: 'Very Poor', 1.5:'Poor', 2: 'Below Average', 2.5: 'Average', 3: 'Above Average', 3.5:'Good', 4: 'Very Good', 4.5:'Excellent' ,5: 'Perfect'},
    // starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'}
});
  $('.red_rating_disabled').rating({
    hoverOnClear: false,
    disabled: true,
    readonly: true,
    displayOnly: true,
    containerClass: 'is-star',
    // step: 0,
    starCaptions: {0.5:'Extremely Poor', 1: 'Very Poor', 1.5:'Poor', 2: 'Below Average', 2.5: 'Average', 3: 'Above Average', 3.5:'Good', 4: 'Very Good', 4.5:'Excellent' ,5: 'Perfect'},
    // starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'}
});



    $('#table_id').DataTable({
      responsive: true
    });

    $("#bootstrap-input").fileinput();


    });


    // var quill = new Quill('#editor', {
    //   modules: {
    //     toolbar: [
    //       [{ 'size': ['small', 'medium', 'large', 'huge'] }],
    //       [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    //       ['bold', 'italic', 'underline'],
    //       ['image'],
    //       [{ 'color': [] }, { 'background': [] }],          ]
    //   },
    //   placeholder: 'Compose an epic...',
    //   theme: 'snow'  // or 'bubble'
    // });





    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

  <div class="se-pre-con"></div>



  <div id="wrapper">


        <!-- Sidebar -->

        <div id="sidebar-wrapper">
            <ul class="sidebar-nav white ">
                <li class="sidebar-brand">
                    <a href="{{ url('/') }}" target="_blank">
                        Review
                    </a>
                </li>
                <li><a href="{{ url('admin/tags') }}">  Front Page</a></li>
                <li><a href="{{ url('admin/items/') }}"> Items</a></li>
                <li><a href="{{ url('admin/custom-rating/') }}"> Custom Rating</a></li>
                <li><a href="{{ url('admin/categories/') }}"> Categories</a></li>
                <li><a href="{{ url('admin/reviews-management') }}"> Moderate </a></li>
                <li><a href="{{ url('admin/reviews') }}"> Your Reviews</a></li>
                <li><a href="{{ url('admin/users') }}">  Users </a></li>
                <li><a href="{{ url('admin/settings/points') }}"> Point Settings </a></li>
                <li><a href="{{ url('admin/blog') }}"> Blog </a></li>
                <li><a href="{{ url('admin/modules') }}"> Modules </a></li>
                <li><a href="{{ url('admin/slideshows') }}"> Slideshows </a></li>

                <li><a href="{{ url('admin/ads') }}">  Ads</a></li>
                <li>
                  <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i>Log Out
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            @yield('content')
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

<script>
    $(document).ready(function(){



    });
</script>

</body>

</html>
