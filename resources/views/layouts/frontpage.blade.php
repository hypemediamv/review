<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review.mv</title>

    <!--======= front page css libs StyleSheet =========-->
    <link href="{{ asset('css/frontpage.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('_override/bootstrap-rating/css/star-rating.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('_override/bootstrap-rating/themes/krajee-svg/theme.css') }}" rel="stylesheet" media="all">



    <!-- front page custom styling -->
    <style>
        body {
            background: rgba(238, 238, 238, 1.0);
        }

        .red-box {
            margin-top:30%;
            text-align: center;
            margin-left: 15%;
            left:50%;

        }

        .red-margin-top{
            margin-top:15px;
        }

        .gp_products_inner {
            box-shadow: none;
            -webkit-box-shadow: none;
        }

        .gp_products_item_caption {
            text-align: center;
        }

        .gp_products_carousel_wrapper {
            background: #FFFFFF;
        }
        .category-listing hr {
            border-color: #e05050;
            border-width: 2pt;
        }

        #loginModal {
            color: #0a0a0a;

        }
    </style>

</head>

<body>

<!-- Header -->
@include('frontpage.partials.header')
        <!-- Page Content -->
@yield('top-slider')


<div class="container-fluid">
    @yield('content')
</div>
<!-- /#page-content-wrapper -->




<!--footer start from here-->
<div class="copyright">
    <div class="container">
        <div class="col-md-6">
            <p> Review.mv ©2016 - All Rights Reserved</p>
        </div>
        <div class="col-md-6">
            <ul class="bottom_ul">
                <li><a href="#">How it Works</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Contact us</a></li>
            </ul>
        </div>
    </div>
</div>

<!--======= front page js libs =========-->
<script src="{{ asset('js/jquery.js') }}"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('js/frontpage.js') }}"></script>
<script type="text/javascript" src="{{ asset('_override/bootstrap-rating/js/star-rating.js') }}"></script>
<script type="text/javascript" src="{{ asset('_override/bootstrap-rating/themes/krajee-svg/theme.js') }}"></script>

<script>
    $('#loginModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Login')
    })
</script>



</body>

</html>
