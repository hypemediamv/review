<!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

<div id="simple_second_carousel" class="carousel simple_second_carousel_fade animate_text simple_second_carousel_wrapper" data-ride="carousel" data-interval="6000" data-pause="hover">

    <!--========= Indicators =========-->
    <ol class="carousel-indicators simple_second_carousel_indicators">
        <li data-target="#simple_second_carousel" data-slide-to="0" class="active"></li>
        <li data-target="#simple_second_carousel" data-slide-to="1"></li>
        <li data-target="#simple_second_carousel" data-slide-to="2"></li>
    </ol>

    <!-- ========= Wrapper for slides  =========-->
    <div class="carousel-inner" role="listbox">

        <!--========= First slide =========-->
        <div class="item active">
            <img src="https://source.unsplash.com/category/Objects/1610x600" alt="slider 01" />
            <div class="carousel-caption simple_second_carousel_caption" data-animation="animated fadeInUp">
                <h2 data-animation="animated fadeInUp">Powerful Packed Designs</h2>
                <h1 data-animation="animated fadeInUp">Designed for Any Website!</h1>
                <a href="#" class="simple_second_carousel_button_colored" data-animation="animated fadeInUp">Read More</a>
            </div>
        </div>

        <!--========= Second slide =========-->
        <div class="item">
            <img src="https://source.unsplash.com/category/nature/1600x600" alt="slider 02" />
            <div class="carousel-caption simple_second_carousel_caption" data-animation="animated fadeInUp">
                <h2 data-animation="animated fadeInUp">Powerful Packed Designs</h2>
                <h1 data-animation="animated fadeInUp">Designed for Any Website!</h1>
                <a href="#" class="simple_second_carousel_button_colored" data-animation="animated fadeInUp">Read More</a>
            </div>
        </div>

        <!--========= Third slide =========-->
        <div class="item">
            <img src="https://source.unsplash.com/category/people/1600x600" alt="slider 03" />
            <div class="carousel-caption simple_second_carousel_caption" data-animation="animated fadeInUp">
                <h2 data-animation="animated fadeInUp">Powerful Packed Designs</h2>
                <h1 data-animation="animated fadeInUp">Designed for Any Website!</h1>
                <a href="#" class="simple_second_carousel_button_colored" data-animation="animated fadeInUp">Read More</a>
            </div>
        </div>

    </div>

    <!--======= Navigation Buttons =========-->

    <!--======= Left Button =========-->
    <a class="left carousel-control simple_second_carousel_control_left" href="#simple_second_carousel" role="button" data-slide="prev">
        <span class="fa fa-angle-double-left simple_second_carousel_control_icons" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <!--======= Right Button =========-->
    <a class="right carousel-control simple_second_carousel_control_right" href="#simple_second_carousel" role="button" data-slide="next">
        <span class="fa fa-angle-double-right simple_second_carousel_control_icons" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->