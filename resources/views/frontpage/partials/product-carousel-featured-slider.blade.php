<!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

<div id="adv_gp_products_4_columns_carousel" class="carousel slide four_shows_one_move gp_products_carousel_wrapper" data-ride="carousel">

    <div class="gp_products_carousel_header">
        <span>Featured Products</span>
        <a href="#" class="pull-right">See more</a>
    </div>

    <!--========= Wrapper for slides =========-->
    <div class="carousel-inner" role="listbox">

        <!--========= 1st slide =========-->
        <div class="item active">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?objects,camera" alt="gp product 001" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 1</a></li>
                            <li><a href="#">Product 1 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--========= 2nd slide =========-->
        <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?objects,watch" alt="gp product 002" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 2</a></li>
                            <li><a href="#">Product 2 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--========= 3rd slide =========-->
        <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?objects,books" alt="gp product 003" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 3</a></li>
                            <li><a href="#">Product 3 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--========= 4th slide =========-->
        <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?objects,chair" alt="gp product 004" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 4</a></li>
                            <li><a href="#">Product 4 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--========= 5th slide =========-->
        <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?nature,sea" alt="gp product 005" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 5</a></li>
                            <li><a href="#">Product 5 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--========= 6th slide =========-->
        <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?nature,tree" alt="gp product 006" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 6</a></li>
                            <li><a href="#">Product 6 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--========= 7th slide =========-->
        <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?objects,paper" alt="gp product 007" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 7</a></li>
                            <li><a href="#">Product 7 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--========= 8th slide =========-->
        <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                <div class="gp_products_inner">
                    <div class="gp_products_item_image">
                        <a href="#">
                            <img src="https://source.unsplash.com/250x250/?objects,table" alt="gp product 008" />
                        </a>
                    </div>
                    <div class="gp_products_item_caption">
                        <ul class="gp_products_caption_name">
                            <li><a href="#">Product 8</a></li>
                            <li><a href="#">Product 8 Description</a></li>
                        </ul>
                        <ul class="gp_products_caption_rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li class="pull-right"><a href="#">view</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!--======= Navigation Buttons =========-->

    <!--======= Left Button =========-->
    <a class="left carousel-control gp_products_carousel_control_left" href="#adv_gp_products_4_columns_carousel" role="button" data-slide="prev">
        <span class="fa fa-angle-left gp_products_carousel_control_icons" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <!--======= Right Button =========-->
    <a class="right carousel-control gp_products_carousel_control_right" href="#adv_gp_products_4_columns_carousel" role="button" data-slide="next">
        <span class="fa fa-angle-right gp_products_carousel_control_icons" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->
