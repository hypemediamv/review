<!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

<div id="gp_products_3_columns_carousel" class="carousel slide gp_products_carousel_wrapper" data-ride="carousel" data-interval="2000">
<div class="row">
    <div class="col-md-3">
        @include('frontpage.partials.category-listing')
    </div>

    <div class="col-md-9">
        <!--========= Wrapper for slides =========-->
        <div class="carousel-inner" role="listbox">

            <!--========= 1st slide =========-->
            <div class="item active">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 gp_products_item">
                        <div class="gp_products_inner">
                            <div class="gp_products_item_image">
                                <a href="#">
                                    <img src="https://source.unsplash.com/250x250/?objects,camera" alt="gp product 001" />
                                </a>
                            </div>
                            <div class="gp_products_item_caption">
                                <ul class="gp_products_caption_name">
                                    <li><a href="#">Product 1</a></li>
                                    <li><a href="#">Product 1 Description</a></li>
                                </ul>
                                <ul class="gp_products_caption_rating">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                    <li class="pull-right"><a href="#">view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 gp_products_item">
                        <div class="gp_products_inner">
                            <div class="gp_products_item_image">
                                <a href="#">
                                    <img src="https://source.unsplash.com/250x250/?objects,watch" alt="gp product 002" />
                                </a>
                            </div>
                            <div class="gp_products_item_caption">
                                <ul class="gp_products_caption_name">
                                    <li><a href="#">Product 2</a></li>
                                    <li><a href="#">Product 2 Description</a></li>
                                </ul>
                                <ul class="gp_products_caption_rating">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                    <li class="pull-right"><a href="#">view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 gp_products_item">
                        <div class="gp_products_inner">
                            <div class="gp_products_item_image">
                                <a href="#">
                                    <img src="https://source.unsplash.com/250x250/?objects,books" alt="gp product 003" />
                                </a>
                            </div>
                            <div class="gp_products_item_caption">
                                <ul class="gp_products_caption_name">
                                    <li><a href="#">Product 3</a></li>
                                    <li><a href="#">Product 3 Description</a></li>
                                </ul>
                                <ul class="gp_products_caption_rating">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                    <li class="pull-right"><a href="#">view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--========= 2nd slide =========-->
            <div class="item">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 gp_products_item">
                        <div class="gp_products_inner">
                            <div class="gp_products_item_image">
                                <a href="#">
                                    <img src="https://source.unsplash.com/250x250/?objects,chair" alt="gp product 004" />
                                </a>
                            </div>
                            <div class="gp_products_item_caption">
                                <ul class="gp_products_caption_name">
                                    <li><a href="#">Product 4</a></li>
                                    <li><a href="#">Product 4 Description</a></li>
                                </ul>
                                <ul class="gp_products_caption_rating">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                    <li class="pull-right"><a href="#">view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 gp_products_item">
                        <div class="gp_products_inner">
                            <div class="gp_products_item_image">
                                <a href="#">
                                    <img src="https://source.unsplash.com/250x250/?nature,sea" alt="gp product 005" />
                                </a>
                            </div>
                            <div class="gp_products_item_caption">
                                <ul class="gp_products_caption_name">
                                    <li><a href="#">Product 5</a></li>
                                    <li><a href="#">Product 5 Description</a></li>
                                </ul>
                                <ul class="gp_products_caption_rating">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                    <li class="pull-right"><a href="#">view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 gp_products_item">
                        <div class="gp_products_inner">
                            <div class="gp_products_item_image">
                                <a href="#">
                                    <img src="https://source.unsplash.com/250x250/?nature,tree" alt="gp product 006" />
                                </a>
                            </div>
                            <div class="gp_products_item_caption">
                                <ul class="gp_products_caption_name">
                                    <li><a href="#">Product 6</a></li>
                                    <li><a href="#">Product 6 Description</a></li>
                                </ul>
                                <ul class="gp_products_caption_rating">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                    <li class="pull-right"><a href="#">view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!--======= Navigation Buttons =========-->

        <!--======= Left Button =========-->
        <a class="left carousel-control gp_products_carousel_control_left" href="#gp_products_3_columns_carousel" role="button" data-slide="prev">
            <span class="fa fa-angle-left gp_products_carousel_control_icons" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <!--======= Right Button =========-->
        <a class="right carousel-control gp_products_carousel_control_right" href="#gp_products_3_columns_carousel" role="button" data-slide="next">
            <span class="fa fa-angle-right gp_products_carousel_control_icons" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>


</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->