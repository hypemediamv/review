
<!-- /#sidebar-wrapper -->
<div id="review-navbar">
    <div class="container">
        <div class="row row1">
            <ul class="largenav pull-right">
                <li class="upper-links"><a class="links" href="#">How it works</a></li>
                <li class="upper-links"><a class="links" href="#">Advertise with us</a></li>

                <li class="upper-links"><a class="links" href="#">News & Events</a></li>

                <li class="upper-links"><a class="links" href="#">Blog</a></li>
                <li class="upper-links">
                    <a class="links" href="#">
                        <svg width="16px" height="12px" style="overflow: visible;">
                            <path d="M8.037 17.546c1.487 0 2.417-.93 2.417-2.417H5.62c0 1.486.93 2.415 2.417 2.415m5.315-6.463v-2.97h-.005c-.044-3.266-1.67-5.46-4.337-5.98v-.81C9.01.622 8.436.05 7.735.05 7.033.05 6.46.624 6.46 1.325v.808c-2.667.52-4.294 2.716-4.338 5.98h-.005v2.972l-1.843 1.42v1.376h14.92v-1.375l-1.842-1.42z" fill="#fff"></path>
                        </svg>
                    </a>
                </li>
                @if(\Auth::check())
                <li class="upper-links dropdown"><a class="links" href="#">{{ \Auth::user()->name }}</a>
                    <ul class="dropdown-menu">
                        <li class="profile-li"><a class="profile-links" href="#">My Reviews</a></li>
                        <li class="profile-li"><a class="profile-links" href="#">Settings</a></li>
                        <li class="profile-li">
                            <a class="profile-links" href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i>Log Out
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>

                    </ul>
                </li>
                    @else
                    <li class="upper-links"><a class="links" href="{{ url('/login') }}" >Log in </a></li>
                    <li class="upper-links"><a class="links" href="{{ url('/register
                    ') }}">New user</a></li>
                    <li class="upper-links"><a class="links" href="#">Online: {{ count(\Auth::user()) }}</a></li>
                @endif
            </ul>
        </div>
        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ Review</span></h2>
                <h1 style="margin:0px;text-align:center;"><span class="largenav"> <b>Review </b></span></h1>
            </div>
            <div class="review-navbar-search smallsearch col-sm-10 col-xs-11">
                <div class="row">
                    <input class="review-navbar-input col-xs-11" type="" placeholder="Search reviews for products, places and more ,,," name="">
                    <button class="review-navbar-button col-xs-1">
                        <svg  class="review-svg"  width="15px" height="15px">
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #2874f0; padding-top: 10px;">
        <span class="sidenav-heading">Home</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
    <a href="#">How-To</a>
    <a href="#">Register</a>

</div>
