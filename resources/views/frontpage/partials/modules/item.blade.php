<div class="panel panel-default red-margin-top">
    <div class="panel-body">
        <div class="col-md-2">
            <div class="red-box">
                <h1>{{$module->name}} </h1>
                <p> {{$module->description}}</p>
                @if($module->link!= null)
                <a href="{{ $module->link}}" class="btn btn-danger">See more</a>
                @endif

            </div>
        </div>
        <div class="col-md-10">

            <!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

            <div id="{{ str_replace(' ', '_', $module->name)}}" class="carousel slide four_shows_one_move gp_products_carousel_wrapper" data-ride="carousel">


                <!--========= Wrapper for slides =========-->
                <div class="carousel-inner" role="listbox">

                    <!--========= 1st slide =========-->
                    <!-- <div class="item active">
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="#">
                                        <img src="https://source.unsplash.com/250x250/?objects,camera" alt="gp product 001" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="#">Product 1</a></li>
                                        <li><a href="#">Product 1 Description</a></li>
                                    </ul>
                                    <ul class="gp_products_caption_rating">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star-half-o"></i></li>
                                        <li class="pull-right"><a href="#">view</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->


                    @if($module->module_items != '[]')

                    @foreach($module->module_items as $key => $module_item)
                    <!--========= 2nd slide =========-->

                    @if($key == 0)
                    <div class="item active">

                    @else
                    <div class="item">

                    @endif
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="{{ action('HomeController@item_index', $module_item->item->id)}}">
                                        <img src="{{ $module_item->item->item_images[0]->image->path}}" style="height:50%;" alt="gp product 00{{$key}}" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption" style="a:focus{color:red;}">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="{{ action('HomeController@item_index', $module_item->item->id)}}">{{$module_item->item->name}}<a></li>
                                        <!-- <li><a href="#">{!! $module_item->item->description !!}</a></li> -->
                                    </ul>
                                    <h3><i class="fa fa-star 2x"></i> {{$module_item->item->average_user_rating}}</h3><br>

                                    <ul class="gp_products_caption_rating">

                                        <li class="pull-right"><a href="#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach

                    </div>
                    @endif



                    @if($module->module_category != null)

                    @foreach($module->module_category->category->items as $key => $module_item)
                    <!--========= 2nd slide =========-->

                    @if($key == 0)
                    <div class="item active">

                    @else
                    <div class="item">

                    @endif
                        <div class="col-xs-12 col-sm-6 col-md-3 gp_products_item">
                            <div class="gp_products_inner">
                                <div class="gp_products_item_image">
                                    <a href="{{ action('HomeController@item_index', ['item' => $module_item->id ])}}">
                                        <img src="{{ $module_item->item_images[0]->image->path}}" style="height:50%;" alt="gp product 00{{$key}}" />
                                    </a>
                                </div>
                                <div class="gp_products_item_caption">
                                    <ul class="gp_products_caption_name">
                                        <li><a href="{{ action('HomeController@item_index', ['item' => $module_item->id])}}">{{$module_item->name}}<a></li>
                                    </ul>
                                    <h3><i class="fa fa-star 2x"></i> {{$module_item->average_user_rating}}</h3><br>

                                    <ul class="gp_products_caption_rating">

                                        <li class="pull-right"><a href="#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach

                    </div>
                    @endif

                <!--======= Navigation Buttons =========-->

                <!--======= Left Button =========-->
                <a class="left carousel-control gp_products_carousel_control_left" href="#{{ str_replace(' ', '_', $module->name)}}" role="button" data-slide="prev">
                    <span class="fa fa-angle-left gp_products_carousel_control_icons" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <!--======= Right Button =========-->
                <a class="right carousel-control gp_products_carousel_control_right" href="#{{ str_replace(' ', '_', $module->name)}}" role="button" data-slide="next">
                    <span class="fa fa-angle-right gp_products_carousel_control_icons" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->


        </div>

        <!--======= Navigation Buttons =========-->

        <!--======= Left Button =========-->

        <!--======= Right Button =========-->


    </div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->
</div>
