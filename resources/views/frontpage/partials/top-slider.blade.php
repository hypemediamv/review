@if($top_slider!= null)
<!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

<div id="simple_second_carousel" class="carousel simple_second_carousel_fade animate_text simple_second_carousel_wrapper" data-ride="carousel" data-interval="6000" data-pause="hover">

    <!--========= Indicators =========-->
    <ol class="carousel-indicators simple_second_carousel_indicators">
      @foreach($top_slider->slides as $key => $slide)
      @if($key == 0)

        <li data-target="#simple_second_carousel" data-slide-to="{{$key}}" class="active"></li>
        @else
        <li data-target="#simple_second_carousel" data-slide-to="{{$key}}"></li>
        @endif

        @endforeach
    </ol>

    <!-- ========= Wrapper for slides  =========-->
    <div class="carousel-inner" role="listbox">



        @foreach($top_slider->slides as $key => $slide)

        @if($key == 0)
        <!--========= First slide =========-->
        <div class="item active">
        @else
        <!--========= Second slide =========-->
        <div class="item">

        @endif
            <img src="{{$slide->image->path}}" alt="slider 0{{$key}}" />
            <div class="carousel-caption simple_second_carousel_caption" data-animation="animated fadeInUp">
                <h2 data-animation="animated fadeInUp">{{$slide->title}}</h2>
                <h1 data-animation="animated fadeInUp">{{ $slide->description}}</h1>
                <a href="#" class="simple_second_carousel_button_colored" data-animation="animated fadeInUp">Read More</a>
            </div>
        </div>


        @endforeach



    </div>

    <!--======= Navigation Buttons =========-->

    <!--======= Left Button =========-->
    <a class="left carousel-control simple_second_carousel_control_left" href="#simple_second_carousel" role="button" data-slide="prev">
        <span class="fa fa-angle-double-left simple_second_carousel_control_icons" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <!--======= Right Button =========-->
    <a class="right carousel-control simple_second_carousel_control_right" href="#simple_second_carousel" role="button" data-slide="next">
        <span class="fa fa-angle-double-right simple_second_carousel_control_icons" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->
@else
@include('frontpage.partials.top-slider-backup')
@endif
