@extends('layouts.frontpage')
@section('content')

<div class="panel panel-default red-margin-top">
    <div class="panel-body">
        <div class="col-md-4">

    <img id="product_view_image" src="{{$item->item_images[0]->image->path}}" width="400" data-zoom-image="{{ $item->item_images[0]->image->path}}"/>
    <div id="product_view_gallery">
<br>
    @foreach($item->item_images as  $item_image)
    <a href="#" data-image="{{$item_image->image->path}}" data-zoom-image="{{$item_image->image->path}}">
        <img id="product_view_image" src="{{$item_image->image->path}}" width="100"/>
    </a>
    @endforeach
  </div>

  </div>
  <div class="col-md-8">

    <h1>  {{ $item->name }}</h1>
    <h3>{{$item->average_user_rating}} <i class="fa fa-star 2x"></i>  ({{$item->user_count}} Reviews)</h3>
    {!! $item->description !!}


</div>
</div>
</div>
</div>

@if($item->reviews != '[]')

<br>

<div class="container-fluid">
<div class="panel panel-default red-margin-top">
    <div class="panel-body">

  @foreach($item->reviews as $review)
    <h3> {{$review->rating}}  <i class="fa fa-star 2x"></i> | {{$review->title}}</h3>
    <h5>A review by <strong>  {{$review->user->name}} </strong> on {{$review->created_at->format('d F Y')}}</h5>
     <input  name="rating" class="red_rating_disabled kv-ltr-theme-default-star rating-loading" value="{{$review->rating}}" dir="ltr" data-size="xs">
{!! $review->review !!}

@endforeach


</div>
</div>
</div>


@else

<div class="alert alert-warning text-center">
<h4> Don't have any reviews yet. Be the First to Review this product, Please <a href="{{ url('/login') }}">login</a> to review </h4>
</div>
@endif
@endsection
