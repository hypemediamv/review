@extends('layouts.frontpage')

<style>
    .category-listing {
        padding: 20px;
        color: #000000;
        overflow: hidden;
    }

    .item {
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        background-color: #FFFFFF;
    }

</style>

@section('content')
    <div class="row well">
        <div class="item">
            <div class="row">
                <div class="category-listing">
                    <p class="text-center">
                        <a href="{{ url('/') }}" class="btn btn-danger "><i class="fa fa-home"> Home</i></a>
                    </p>
                    <h3 class="text-center">Category Name</h3>
                    <p class="text-mute text-small text-center">8 items</p>
                    <hr>
                </div>
            </div>
            <div class="row">
                @for($i = 0; $i < 8; $i++)
                    <div class="col-xs-12 col-sm-3 col-md-3 gp_products_item">
                        <div class="gp_products_inner">
                            <div class="gp_products_item_image">
                                <a href="#">
                                    <img src="https://source.unsplash.com/250x250/?nature,tree" alt="gp product 004" />
                                </a>
                            </div>
                            <div class="gp_products_item_caption">
                                <ul class="gp_products_caption_name">
                                    <li><a href="#">Product {{$i}}</a></li>
                                    <li><a href="#">Product {{$i}} Description</a></li>
                                </ul>
                                <ul class="gp_products_caption_rating">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                    <li class="pull-right"><a href="#">view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endfor

            </div>

        </div>
    </div>
@endsection