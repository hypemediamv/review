@extends('layouts.dashboard')
@section('content')


@include('partials.alerts')

@if(Session::get('edit_category') != null)

<div class="well">
  <a href="{{ action('CategoryController@index')}}" ><div class="btn btn-default  pull-right">Close </div></a>
<h1> Edit Category | <strong> {{ Session::get('edit_category')->name }}  </strong></h1>

{!! Form::open(['action' => ['CategoryController@update', Session::get('edit_category')->id ],  'method' => 'patch' ,'style' => 'display:inline;']) !!}
@include('dashboard/category/partials/form',['button_text' => 'Update','edit_category' => Session::get('edit_category') ])

{!! Form::close() !!}

<button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal">Delete</button>
<!-- Modal -->
<div id="delete-modal" class="modal fade" role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Delete Confirmation </h4>
    </div>
    <div class="modal-body">
    <p>  Are you sure you want to delete <b> " {{Session::get('edit_category')->name}}  "  </b> ?</p>
      </div>
    <div class="modal-footer">
      {!! Form::open(['action' => ['CategoryController@destroy', Session::get('edit_category')->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
      {{ csrf_field()  }}
      <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
      {{Form::close() }}
      <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
  </div>

</div>
</div>


</div>

@else

<h1> Add Category </h1>
{!! Form::open(['action' => ['CategoryController@store'],  'method' => 'post']) !!}
@include('dashboard/category/partials/form',['button_text' => 'Add'])

{!! Form::close() !!}

@endif



@if($categories != '[]')

<h2>Existing Categories
  <button type="button" class="btn btn-default  pull-right" data-toggle="modal" data-target="#show-tree-modal">Show Category Hierachy</button>
</h2>

<table class="table table-hover " id="table_id">
    <thead>
      <tr>
        <th>Category Name </th>
        <th>Sub Categories</th>

        <th class="col-sm-1">
          Created By

        </th>
        <th class="col-sm-1">Created At</th>
        <th class="col-sm-1">Functions</th>


      </tr>
    </thead>
    <tbody>

      @foreach($categories as $key => $category)
      <tr>
        <td>
          <a href="{{ action('CategoryController@edit', $category->id ) }}"><strong>{{$category->name}} </strong></a>
          <p> <small>{{$category->description}}</small></p>
        </td>
        <td>
          @if($category->children != null)
          <ul>
            @foreach($category->children as $children)
            <li>  <a href="{{ action('CategoryController@edit', $children->id ) }}">{{ $children->name}} </a></li>
            @endforeach
          </ul>
          @endif

        </td>


        <td>{{ $category->user->name }} </td>
        <td>{{ $category->created_at->diffForHumans()}} </td>

        <td>
          <a href="{{ action('CategoryController@edit', $category->id ) }}">
          <button type="button" class="btn btn-default ">Edit</button>
          </a>

          <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
          <!-- Modal -->
        <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation </h4>
              </div>
              <div class="modal-body">

              <p>  Are you sure you want to delete <b> " {{$category->name}}  "  </b> ?</p>

                    </div>
              <div class="modal-footer">
                {!! Form::open(['action' => ['CategoryController@destroy', $category->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                {{ csrf_field()  }}
                <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                {{Form::close() }}
                <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
            </div>

          </div>
        </div>



        </td>


      </tr>
      @endforeach

    </tbody>
  </table>



  <div id="show-tree-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Category Hierachy </h4>
        </div>
        <div class="modal-body">

          @php
          echo('<ul class="tree">');

              $traverse = function ($categories, $prefix = '<li>', $suffix = '</li>') use (&$traverse) {
                  foreach ($categories as $category) {
                      echo $prefix.$category->name.$suffix;

                      $hasChildren = (count($category->children) > 0);

                      if($hasChildren) {
                          echo('<ul>');
                      }

                      $traverse($category->children);

                      if($hasChildren) {
                          echo('</ul>');
                      }
                  }
              };

              $traverse($category_tree);

              echo('</ul>');

          @endphp
              </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">Close</button>      </div>
      </div>

    </div>
  </div>


@endif

@endsection
