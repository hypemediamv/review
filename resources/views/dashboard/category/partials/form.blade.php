
<div class="form-group">
  <label for="text">Category Name:</label>
  @if(isset($edit_category))
  <input type="text" name="name" class="form-control" value="{{ $edit_category->name }}" required >
  @else
  <input type="text" name="name" class="form-control" value="{{ request()->input('name', old('name'))  }}" required >
  @endif
</div>

<div class="form-group">
<label for="text">Category Parent:</label>
@if(isset($edit_category) && isset($edit_category->parent->id))
{!! Form::select('parent', $parent_categories , $edit_category->parent->id, ['class' => 'form-control']) !!}
@else
{!! Form::select('parent', $parent_categories , null, ['class' => 'form-control select2']) !!}
@endif
</div>

<div class="form-group">
  <label for="text">Category Description:</label>
  @if(isset($edit_category))
  <input type="text" name="description" class="form-control" value="{{  $edit_category->description }} " required>
  @else
  <input type="text" name="description" class="form-control" value="{{ request()->input('description', old('description')) }} " required>
  @endif



</div>
<br>
<button type="submit" class="btn btn-default">{{ $button_text }}</button>
