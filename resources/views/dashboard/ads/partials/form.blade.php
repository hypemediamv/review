@extends('layouts.dashboard')
@section('content')
    @include('partials.alerts')

    @if($ads != null)

        <div class="well">
            <a href="{{ action('AdsController@index')}}" ><div class="btn btn-default pull-right">Close </div></a>

            <h1>Edit Ads | <strong> {{$ads->name}}</strong> </h1>

            {!! Form::open(['action' => ['AdsController@update', $ads->id ],  'method' => 'patch', 'files' => 'true']) !!}
            {{ csrf_field()  }}

            <div class="form-group">
                <label for="text">Ads Name:</label>
                <input type="text" name="name" class="form-control" value="{{ $ads->name}}" required>
            </div>
            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" id="bootstrap-input"/>
                <span class="label label-info">To replace existing image select new</span>
            </div>

            <div class = "thumbnail responsive">
                <img src="{{ $ads->image->path }}"  width="50%" height="50%">
            </div>
            <div class="form-group">
                <label for="text">Ads Owner:</label>
                <input type="text" name="owner" class="form-control"  value="{{ $ads->owner }}" required>
            </div>
            <div class="form-group">
                <label for="text">Ads Expiry Date:</label>
                <input type="date" name="expiry_date" class="form-control" value="{{ $ads->expiry_date }}" required>
            </div>

            <br>
            <button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i> Update</button>
            {{Form::close() }}

        </div><!--well-->





    @endif
@endsection
