@extends('layouts.dashboard')
@section('content')
@include('partials.alerts')

@if($review != null)

<div class="well">
  <a href="{{ action('ReviewController@index')}}" ><div class="btn btn-default pull-right">Close </div></a>

  <h1>Edit Review for  | <strong> {{$review->item->name}}</strong> </h1>

  {!! Form::open(['action' => ['ReviewController@update', $review->id ],  'method' => 'patch']) !!}
  {{ csrf_field()  }}


  @if(Auth::user()->is_expert == 1)

  <h3> As an Expert User, You can add Custom Ratings to your review </h3>

    @if($custom_ratings != '[]')

    {!! Form::select('custom_rating_list',  $custom_ratings->pluck('name','id')  , $selected_custom_ratings, ['id'=>'custom_rating_list', 'name'=>'custom_rating_list[]','class'=>'form-control select2','multiple']) !!}

  <button formnovalidate="formnovalidate" type="submit" name="add_custom_rating" class="btn btn-default"> <strong>Add Custom Rating </strong></button>



    @else

    <div class="alert alert-warning">
      Looks like you do not have any custom ratings added to rate
    </div>

    @endif


  @endif



  <div class="form-group">
    <label for="text">Choose an item to review:</label>
    {!! Form::select('item_id', $items->pluck('name','id') , $review->item->name, ['class' => 'form-control select2']) !!}
  </div>

  <div class="form-group">
    <label for="text">Title:</label>
    <input name="title" type="text" class="form-control" required value="{{$review->title }}">

  </div>



    <div class="form-group">
      <label for="text">Overeall Rating:</label>
      <input class="red_rating" name="rating" class="kv-ltr-theme-default-star rating-loading" value="{{$review->rating}}" dir="ltr" data-size="xs">
    </div>


  @if(Auth::user()->is_expert == 1)
    @if($custom_ratings != '[]')
    @foreach($review->expert_custom_ratings as $key => $expert_custom_rating )
    <div class="form-group">
      <label for="text">{{$expert_custom_rating->custom_rating->name}}</label>
      <input class="red_rating"  name="selected_custom_ratings[{{$expert_custom_rating->custom_rating->id}}]" class="kv-ltr-theme-default-star rating-loading" value="{{$expert_custom_rating->rating}}" dir="ltr" data-size="xs">
    </div>
    @endforeach
    @endif
  @endif











  @if(isset( $_POST['publish'] ))
  <input  name="publish" type="hidden" value="1">
  @endif

  <div class="form-group">
    <label for="text">Review:</label>
    <textarea id="froala-editor-full" name="review" required> {!! $review->review !!}</textarea>

  </div>
<br>

@if($review->status->name == 'draft')
  <button type="submit" name="publish" class="btn btn-success"> <strong>Publish </strong></button>
  <button type="submit" name="draft" class="btn btn-default">Save As Draft</button>
  @else
  <button type="submit" disabled name="publish" class="btn btn-success disabked"> <strong>Publish </strong></button>
  <button type="submit" disabled name="draft" class="btn btn-default disabled">Save As Draft</button>

@endif
  {{Form::close() }}


</div><!--well-->





@endif
@endsection
