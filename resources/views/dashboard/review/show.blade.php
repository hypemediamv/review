@extends('layouts.dashboard')
@section('content')
@include('partials.alerts')

@if($review->user->is_expert == 1)
<span class="label label-success">An Expert Review</span>

@endif

@include('dashboard.item.partials.item',['item' => $review->item])


<br>


<div class="well">
  <div class="pull-right"> <h4> Review Status : <strong>{{ $review->status->name }} </strong>  </h4> </div>
    <h3>{{$review->title}}</h3>
    <h5>A review by <strong>  {{$review->user->name}} </strong> on {{$review->created_at->format('d F Y')}}</h5>


     <input  name="rating" class="red_rating_disabled kv-ltr-theme-default-star rating-loading" value="{{$review->rating}}" dir="ltr" data-size="xs">
     @if($review->expert_custom_ratings != '[]')



     <div class="row alert alert-warning">

       @foreach($review->expert_custom_ratings as $expert_custom_rating)

     <div class="col-md-1">
      <span> <h4> {{$expert_custom_rating->custom_rating->name}} </h4>
        <input  name="rating" class="red_rating_disabled kv-ltr-theme-default-star rating-loading" value="{{$expert_custom_rating->rating}}" dir="ltr" data-size="xxs">
      </span>
     </div>
       @endforeach
     </div>
     @endif




    <hr>
{!! $review->review !!}


  </div>




@endsection
