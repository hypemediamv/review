




{!! Form::open(['action' => ['ReviewController@store'],  'method' => 'post', 'name' => 'editor_form']) !!}

@if(Auth::user()->is_expert == 1)

<h3> As an Expert User, You can add Custom Ratings to your review </h3>

  @if($custom_ratings != '[]')

  {!! Form::select('custom_rating_list', $custom_ratings->pluck('name','id'), null, ['id'=>'custom_rating_list', 'name'=>'custom_rating_list[]','class'=>'form-control select2','multiple']) !!}

<button formnovalidate="formnovalidate" type="submit" name="add_custom_rating" class="btn btn-default"> <strong>Add Custom Rating </strong></button>



  @else

  <div class="alert alert-warning">
    Looks like you do not have any custom ratings added to rate
  </div>

  @endif


@endif
<hr>


  <div class="form-group">
    <label for="text">Choose an item to review:</label>
    {!! Form::select('item_id', $items->pluck('name','id') ,null, ['class' => 'form-control select2']) !!}
  </div>
  <div class="form-group">
    <label for="text">Title:</label>
    <input name="title" type="text" class="form-control" required value="{{ old('title')}}">

  </div>

<div class="row">
  <div class="form-group col-md-3">
    <label for="text">Rating:</label>
    <input id="red_rating" name="rating"  value="0" dir="ltr" data-size="xs" value="{{ old('rating')}}">

  </div>

  @if(Auth::user()->is_expert == 1)
  @if(Session::get('selected_custom_ratings') != null)


  @foreach(Session::get('selected_custom_ratings') as $key => $custom_rating )

  <div class="form-group col-md-3">
    <label for="text">{{$custom_rating->name}}</label>
    <input class="red_rating" name="selected_custom_ratings[{{$custom_rating->id}}]"  dir="ltr" data-size="xs">

  </div>


  @endforeach

  @endif
  @endif




</div>

  @if(isset( $_POST['publish'] ))
  <input  name="publish" type="hidden" value="1">
  @endif

  <div class="form-group">
    <label for="text">Review:</label>

    <textarea id="froala-editor-full" name="review" required> {{old('review')}}</textarea>

  </div>
<br>



  <button type="submit" name="publish" class="btn btn-success"> <strong>Publish </strong></button>
  <button type="submit" name="draft" class="btn btn-default">Save As Draft</button>

{!! Form::close() !!}
