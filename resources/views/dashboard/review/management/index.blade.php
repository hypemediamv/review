@extends('layouts.dashboard')

@section('content')
@include('partials.alerts')


@if($reviews != '[]')
<h2> All Reviews </h2>

<table class="table table-hover" id="table_id">
    <thead>
      <tr>
        <th  class="col-sm-1">Rating</th>

        <th  class="col-sm-2">Item Name </th>
        <th>Review</th>
        <th  class="col-sm-1">Status</th>
        <th  class="col-sm-1">Created At</th>
        <th  class="col-sm-1">Moderated By</th>

        <th> Functions</th>
        <th> Moderate</th>

      </tr>
    </thead>
    <tbody>


      @foreach($reviews as $key => $review)
      <tr>
        <td> <input  name="rating" class="red_rating_disabled kv-ltr-theme-default-star rating-loading" value="{{$review->rating}}" dir="ltr" data-size="xs">
<div style="display:none"> {{$review->rating}}  </div>
        </td>

        <td> <a href="{{ action('ItemController@show', $review->id ) }}">{{$review->title}} {{$review->item->name}} </a></td>
        <!-- <td> {!! str_limit( $review->review, $limit = 100, $end = '...') !!} </td> -->
        <td><a href="{{ action('ReviewController@show', $review->id ) }}">{{$review->title}} </a></td>
        <td>
          @include('dashboard.partials.status',['status' => $review->status])
        </td>
        <td>{{ $review->created_at->diffForHumans()}} </td>
        <td></td>
        <td>


          <a href="{{ action('ReviewController@edit', $review->id ) }}">
              <button type="button" class="btn btn-default ">Edit</button>
          </a>


            <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
            <!-- Modal -->
          <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Delete Confirmation </h4>
                </div>
                <div class="modal-body">

                <p>  Are you sure you want to delete this review for  <b> " {{$review->item->name}}  "  </b> ?</p>

                      </div>
                <div class="modal-footer">

                  <button type="submit" name="delete" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                  <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
              </div>

            </div>
          </div>








        </td>
        <td>


          {!! Form::open(['action' => ['ReviewManagementController@update', $review->id ],  'method' => 'PATCH' ,'style' => 'display:inline;']) !!}
          {!! method_field('patch') !!}
              <button type="submit"  name="draft" class="btn btn-default" style="display:inline-block;"> <strong> Draft </strong></button>
              <button type="submit" name="pending" class="btn btn-warning" style="display:inline-block;"> <strong> Pend </strong></button>
              <button type="submit" name="reject" class="btn btn-danger" style="display:inline-block;"> <strong> Reject </strong></button>
              <button type="submit" name="approve" class="btn btn-success" style="display:inline-block;"> <strong> Approve </strong></button>

          {!! Form::close() !!}
        </td>
      </tr>
      @endforeach

    </tbody>
  </table>



@endif

@endsection
