@extends('layouts.dashboard')
@section('content')

@include('partials.alerts')

<h1 style="display:inline-block">&nbsp; Add Review </h1>
@include('dashboard.review.partials.create')


<hr>

@if($reviews != '[]')

<h2> Your Reviews </h2>

<table class="table table-hover" id="table_id">
    <thead>
      <tr>
        <th  class="col-sm-1">Rating</th>

        <th  class="col-sm-2">Item Name </th>
        <th>Review</th>
        <th  class="col-sm-1">Status</th>
        <th  class="col-sm-1">Created At</th>
        <th  class="col-sm-2"> Functions</th>

      </tr>
    </thead>
    <tbody>


      @foreach($reviews as $key => $review)
      <tr>
        <td> <input  name="rating" class="red_rating_disabled kv-ltr-theme-default-star rating-loading" value="{{$review->rating}}" dir="ltr" data-size="xs">
<div style="display:none"> {{$review->rating}}  </div>
        </td>

        <td> <a href="{{ action('ItemController@show', $review->item->id ) }}">{{$review->title}} {{$review->item->name}} </a></td>
        <!-- <td> {!! str_limit( $review->review, $limit = 100, $end = '...') !!} </td> -->
        <td><a href="{{ action('ReviewController@show', $review->id ) }}">{{$review->title}} </a></td>
        <td>
          @include('dashboard.partials.status',['status' => $review->status])
        </td>
        <td>{{ $review->created_at->diffForHumans()}} </td>
        <td>

          @if($review->status->name == 'draft')

          <a href="{{ action('ReviewController@edit', $review->id ) }}">
              <button type="button" class="btn btn-default ">Edit</button>
          </a>


            <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
            <!-- Modal -->
          <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Delete Confirmation </h4>
                </div>
                <div class="modal-body">

                <p>  Are you sure you want to delete this review for  <b> " {{$review->item->name}}  "  </b> ?</p>

                      </div>
                <div class="modal-footer">
                  {!! Form::open(['action' => ['ReviewController@destroy', $review->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                  {{ csrf_field()  }}
                  <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                  {{Form::close() }}
                  <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
              </div>

            </div>
          </div>

          {!! Form::open(['action' => ['ReviewController@publish', $review->id ],  'method' => 'PATCH' ,'style' => 'display:inline;']) !!}
          {!! method_field('patch') !!}
              <input type="hidden" value="{{ $review->id}}" name="review_id">
              <button type="submit" class="btn btn-success "> <strong> Publish </strong></button>
          {!! Form::close() !!}

          @else
          <button type="button"  disabled class="btn btn-default disabled ">Edit</button>
          <button type="button"  disabled class="btn btn-default disabled ">Delete</button>
          <button type="button"  disabled class="btn btn-default disabled ">Published</button>


          @endif




        </td>
      </tr>
      @endforeach

    </tbody>
  </table>



@endif

@endsection
