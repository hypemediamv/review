{!! Form::open(['action' => ['ItemController@store'],  'method' => 'post', 'files'=> 'true', 'name' => 'editor_form']) !!}
  <div class="form-group">
    <label for="text">Item Name:</label>
    <input type="text" name="name" class="form-control" id="email" required>
  </div>
  <div class="form-group">
    <label for="text">Item Description:</label>

    <textarea id="froala-editor" name="description" required></textarea>

  </div>

  <div class="form-group">
    <label for="item_category">Item Category</label>
    {!! Form::select('category_id', $categories->pluck('name','id') ,null, ['class' => 'form-control select2']) !!}
  </div>

<div class="form-group">
  <label for="item_images">Image(s)</label>
  <input type="file" name="item_images[]" id="bootstrap-input" multiple required/>
</div>
<br>
  <button type="submit" class="btn btn-default">Submit</button>
{!! Form::close() !!}
