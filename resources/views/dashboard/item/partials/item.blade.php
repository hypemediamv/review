<div class="row">

  <div class="col-md-4">
    <img id="product_view_image" src="{{$item->item_images[0]->image->path}}" width="400" data-zoom-image="{{ $item->item_images[0]->image->path}}"/>
    <div id="product_view_gallery">
<br>
    @foreach($item->item_images as  $item_image)
    <a href="#" data-image="{{$item_image->image->path}}" data-zoom-image="{{$item_image->image->path}}">
        <img id="product_view_image" src="{{$item_image->image->path}}" width="100"/>
    </a>
    @endforeach
  </div>

  </div>
  <div class="col-md-8">

    <h1>{{ $item->name }}</h1>

    {!! $item->description !!}


</div>
</div>
