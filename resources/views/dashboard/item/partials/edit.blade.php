@extends('layouts.dashboard')
@section('content')
@include('partials.alerts')

@if($item != null)

<div class="well">
  <a href="{{ action('ItemController@index')}}" ><div class="btn btn-default pull-right">Close </div></a>

  <h1>Edit item | <strong> {{$item->name}}</strong> </h1>

  {!! Form::open(['action' => ['ItemController@update', $item->id ],  'method' => 'patch', 'files' => 'true']) !!}
  {{ csrf_field()  }}

  <div class="form-group">
    <label for="text">Item Name:</label>
    <input type="text" name="name" class="form-control" value="{{ $item->name}}" required>
  </div>
  <div class="form-group">
    <label for="text">Item Description:</label>
    <textarea id="froala-editor" name="description" required>{{ $item->description }}</textarea>
  </div>

<div class="form-group">
  <label for="item_category">Item Category</label>
  {!! Form::select('category_id', $categories->pluck('name','id') , $item->category_id , ['class' => 'form-control']) !!}
</div>

  <div class="form-group">
    <label for="item_images">Image(s)</label>
    <input type="file" name="item_images[]" id="bootstrap-input" multiple />
  </div>
<br>
  <button type="submit" class="btn btn-default">Submit</button>
  {{Form::close() }}

<div class="form-group">
  <br>
  <label for="text">Existing Item Images:</label>
  <div class = "row">


        @foreach($item->item_images as $key => $item_image)


        <div class = "col-sm-6 col-md-3">
           <div class = "thumbnail">
             <img src="{{ $item_image->image->path }}"  width="304" height="236">
           </div>
           <div class="review-item-box">{{$item_image->image->filename}}</div>
           <div class = "caption">




             @if($item->item_images->count() <= 1)

<span class="tool-tip" data-toggle="tooltip" data-placement="top" title="An item needs atleast one image">
      <button type="button" class="btn btn-danger btn-block disabled">Delete</button>
</span>

             @else
              <p>
                <button type="button" class="btn btn-danger btn-block " data-toggle="modal" data-target="#delete-item-image-{{$key}}">Delete</button>
              </p>
              @endif
           </div>
         </div>


            <!-- Modal -->
          <div id="delete-item-image-{{$key}}" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Delete Confirmation  </h4>
                </div>
                <div class="modal-body">
                  <img src="{{ $item_image->image->path }}"  width="100%"><br><br>
                <strong>  Are you sure you want to delete the  image  ?</strong>

                      </div>
                <div class="modal-footer">
                  {!! Form::open(['action' => ['ItemImageController@destroy', $item_image->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                  {{ csrf_field()  }}
                  <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                  {{Form::close() }}
                  <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
              </div>

            </div>
          </div>




        @endforeach

</div><!--form-group-->
</div><!--row-->
</div><!--well-->





@endif
@endsection
