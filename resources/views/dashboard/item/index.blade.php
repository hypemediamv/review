@extends('layouts.dashboard')
@section('content')

@include('partials.alerts')



@if(Session::get('edit_item') != null)
@include('dashboard.item.partials.show')
@else
<h1> Add item </h1>
@include('dashboard.item.partials.create')
@endif

@if($items != '[]')

<h2>Existing Items </h2>

<table class="table table-hover" id="table_id">
    <thead>
      <tr>
        <th >Item Name </th>
        <th  class="col-sm-1">Created By</th>
        <th  class="col-sm-1">Created At</th>
        <th  class="col-sm-1">Functions</th>

      </tr>
    </thead>
    <tbody>

      @foreach($items as $key => $item)
      <tr>
        <td><a href="{{ action('ItemController@show', $item->id ) }}"> <strong>{{$item->name}} </strong></td>
        <td>{{ $item->user->name }} </td>
        <td>{{ $item->created_at->diffForHumans()}} </td>
        <td>


        <a href="{{ action('ItemController@edit', $item->id ) }}">
            <button type="button" class="btn btn-default ">Edit</button>
        </a>


          <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
          <!-- Modal -->
        <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation </h4>
              </div>
              <div class="modal-body">

              <p>  Are you sure you want to delete <b> " {{$item->name}}  "  </b> ?</p>

                    </div>
              <div class="modal-footer">
                {!! Form::open(['action' => ['ItemController@destroy', $item->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                {{ csrf_field()  }}
                <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                {{Form::close() }}
                <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
            </div>

          </div>
        </div>



        </td>
      </tr>
      @endforeach

    </tbody>
  </table>



@endif

@endsection
