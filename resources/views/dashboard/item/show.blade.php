@extends('layouts.dashboard')
@section('content')
@include('partials.alerts')

<h1> <strong>{{$item->name }} </strong></h1>
<div class="well">
@include('dashboard.item.partials.item',['item' => $item])
</div>

@endsection
