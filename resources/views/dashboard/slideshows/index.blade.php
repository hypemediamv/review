@extends('layouts.dashboard')
@section('content')

    @include('partials.alerts')

    <ul class="nav nav-tabs">
        <li ><a data-toggle="tab" href="#add_slideshows"> Slide shows</a></li>
        <li class="active"><a data-toggle="tab" href="#add_slides">Add Slide</a></li>
    </ul>

    <div class="tab-content">
        <div id="add_slideshows" class="tab-pane fade">
            <h1>Add Slide Show</h1>
            {!! Form::open(['action' => ['SlideshowController@store' ],  'method' => 'post', 'files' => 'true']) !!}
                    <div class="form-group">
                        <label for="name"> Slide Show Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Slide Show Name" value="{{ old('name') }}" required/>
                    </div>

                    <div class="form-group">
                        <label for="is_active">is Active:</label>
                        <input type="checkbox" name="is_active" checked value="1">
                    </div>

                    <div class="form-group">
                        <label for="position_id">Slide show position: </label>
                        <select name="position_id" class="form-control select2">
                            <option value="">Select position</option>
                        @foreach($slideshow_positions as $index => $position)
                                <option value="{{$index}}" @if (old('position_id') == $index) selected="selected" @endif>{{ $position }}</option>
                        @endforeach
                        </select>
                    </div>

                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add </button>
                    {!! Form::close() !!}
            <hr>

            <h1>Slide Shows : ({{$slide_shows->count()}})</h1>
            <table class="table table-hover ">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Slide Show Position</th>
                            <th>Active</th>
                            <th>Functions</th>
                        </tr>
                        </thead>
                <tbody>
                @if($slide_shows->count() > 0)
                @foreach($slide_shows as $key => $slide_show)
                <tr>
                    <td> {{ $slide_show->id }}</td>
                    <td> {{ $slide_show->name }}</td>
                    <td>{{ $slide_show->slideShowPosition }}</td>
                    <td> @if($slide_show->is_active == 1) Active <span class="fa fa-check"></span> @else <span class="fa fa-remove"></span>@endif</td>
                            <td>
                                <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-slideshow-{{$key}}">Delete</button>
                            </td>

                    @include('dashboard.partials.delete-modal',['id' => $slide_show->id, 'name' => $slide_show->name, 'controller' => 'SlideshowController', 'modelName' => 'slideshow'])

                </tr>
                @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    <div class="alert alert-info">
                                        <strong><i class="fa fa-exclamation-circle"></i></strong> Their are no slide shows.
                                    </div>
                                </td>

                            </tr>
                        @endif
                </tbody>

            </table>

        </div>


        <div id="add_slides" class="tab-pane fade in active">
            <h1>Add Slide</h1>
            {!! Form::open(['action' => ['SlideController@store' ],  'method' => 'post', 'files' => 'true']) !!}
            <div class="form-group">
                <label for="title"> Slide Title</label>
                <input type="text" name="title" class="form-control" placeholder="Enter Slide Title" value="{{ old('title') }}" required/>
            </div>

            <div class="form-group">
                <label for="description"> Slide Description</label>
                <textarea name="description" class="form-control" required> {{old('description')}}</textarea>
            </div>

            <div class="form-group">
                <label for="slideshow_id">Slide Show Name: </label>
                <select name="slideshow_id" class="form-control select2" required>
                    {{--<option value="">Select slide show</option>--}}
                    @foreach($slide_shows as $slide_show)
                        <option value="{{$slide_show->id}}" @if (old('slideshow_id') == $slide_show->id) selected="selected" @endif>{{ $slide_show->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="image">Select image</label>
                <input type="file" name="image" id="bootstrap-input" required/>
            </div>

            {!! csrf_field() !!}
            <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add </button>
            {!! Form::close() !!}
            <hr>

            <h1>Slides : ({{$slides->count()}})</h1>
            <table class="table table-hover" id="table_id">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Slide Show Name</th>
                    <th>Slide Image</th>
                    <th>Functions</th>
                </tr>
                </thead>
                <tbody>
                @if($slides->count() > 0)
                    @foreach($slides as $key => $slide)
                        <tr>
                            <td> {{ $slide->id }}</td>
                            <td> {{ $slide->title }}</td>
                            <td> {{ $slide->description }}</td>
                            <td> {{ $slide->slideshow->name }}</td>
                            <td><img src="{{ $slide->image->path }}" width="200" height="75" alt=""></td>
                            <td>
                                <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-slide-{{$key}}">Delete</button>
                            </td>

                            @include('dashboard.partials.delete-modal',['id' => $slide->id, 'name' => $slide->title, 'controller' => 'SlideController', 'modelName' => 'slide'])

                        </tr>
                    @endforeach
                @endif
                </tbody>

            </table>

        </div>

    </div>


@endsection
