@extends('layouts.dashboard')
@section('content')


@include('partials.alerts')


@if($point_settings != '[]')

<h2> Point Settings</h2>

<table class="table table-hover ">
    <thead>
      <tr>
        <th>Key </th>
        <th>Value </th>



      </tr>
    </thead>
    <tbody>
      {!! Form::open(['action' => ['PointSettingController@store'],  'method' => 'post']) !!}

      {{ csrf_field()}}
      @foreach($point_settings as  $point_setting)
      <tr>
        <td>
          <strong>{{$point_setting->key}} </strong>
        </td>
        <td>
          <input type="text" value="{{$point_setting->value}}" name="keys[{{$point_setting->key}}]">
          </td>



      </tr>
      @endforeach



    </tbody>
  </table>
  <button type="submit" class="btn btn-default"> Change </button>

  {!! Form::close() !!}
  <h2> Configure Manual Point Rank System </h2>
<br>
  {!! Form::open(['action' => ['PointSettingController@store_level'],  'method' => 'post', 'class' => 'form-inline'] ) !!}

  {{ csrf_field()}}
  <div class="form-group">

  <label for "rank"> Rank for Custom Level </label>
  <input type="text"name="rank" >
</div>

<div class="form-group">

  <label for "base_point_increment_percentage"> Base Point Increment % </label>
  <input type="text"name="base_point_increment_percentage">
</div>
<div class="form-group">

  <label for "level"> Points </label>
  <input type="number"  name="points">
</div>
<button class="btn btn-default" type="submit"> ADD LEVEL </button>

  {!! Form::close() !!}


@endif

@if($point_levels != null)

<h3> Point Levels</h3>

<table class="table table-hover ">
    <thead>
      <tr>
        <th>Rank </th>
        <th>Base Point Increment % </th>
        <th>Points</th>
        <th>Functions</th>


      </tr>
    </thead>
    <tbody>

      @foreach($point_levels as  $level)
      <tr>
        <td>
          <strong>{{$level->rank}} </strong>
        </td>
        <td>
          <strong>{{$level->base_point_increment_percentage}} </strong>
        </td>
        <td>
          <strong>{{$level->points}} </strong>
        </td>
        <td>

          {!! Form::open(['action' => ['PointSettingController@destroy_level'],  'method' => 'post' ,'style' => 'display:inline-block']) !!}
          {{ csrf_field()  }}
          <input name="id" type="hidden" value="{{$level->id}}">
          <button type="submit" class="btn btn-danger"  style="display:inline-block;" >DELETE LEVEL</button>
          {{Form::close() }}
        </td>




      </tr>
      @endforeach

    </tbody>
  </table>

@endif
@endsection
