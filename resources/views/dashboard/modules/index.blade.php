@extends('layouts.dashboard')
@section('content')

@include('partials.alerts')


<h2> Module Assignment </h2>


{!! Form::open(['action' => ['ModuleController@store' ],  'method' => 'post', 'class' => 'form-group']) !!}


<div class="form-group">
  <label for="text">Name / Heading*</label>
  <input type="text" name="name" class="form-control" required></input>
</div>

<div class="form-group">
  <label for="text">Description*</label>
  <input type="text" name="description" class="form-control" required></input>
</div>


<div class="form-group">
  <label for="text">Module Postiion*</label>
  {!! Form::select('module_position_id', $module_positions->pluck('name','id') ,null, ['class' => 'form-control select2']) !!}
</div>
<div class="radio">
  <label><input type="radio" name="module_type" value="category">By Category</label>
</div>
<div class="radio">
  <label><input type="radio" name="module_type" value="item" >By Items</label>
</div>


<div class="form-group">
  <label for="text">By  Items</label>

  {!! Form::select('item_list', $items, null, ['id'=>'item_list', 'name'=>'item_list[]','class'=>'form-control select2','multiple']) !!}

</div>



<div class="form-group">
  <label for="text">By Review Category</label>
  {!! Form::select('review_category_id', $review_categories->pluck('name','id') ,null, ['class' => 'form-control select2']) !!}
</div>

<div class="form-group">
  <label for="text">Limit (Default: 4)</label>
  <input type="text" name="limit" class="form-control"></input>
</div>


<div class="form-group">
  <label for="text">Link</label>
  <input type="text" name="linik" class="form-control"></input>
</div>

<button type="submit" class="btn btn-success"> ADD MODULE </button>


  {!! Form::close() !!}


      @if($modules!= null)
      <h2>Modules </h2>
      <table class="table table-hover" >
          <thead>
            <tr>
              <th> Module Position </th>
              <th >Module Name </th>
              <th>Module Description</th>

              <th>By Category </th>
              <th>By Items </th>
              <th> Limit </th>
              <th> Functions </th>

            </tr>
          </thead>
          <tbody>

            @foreach($modules as $key => $module)
            <tr>
              <td> {{$module->module_position->name}} </td>

              <td> {{$module->name }}</td>


              <td>
                {{ $module->description}}
              </td>
              <td>
                @if($module->module_category != null)
                {{ $module->module_category->category->name}}

                @else
                -
                @endif
              </td>
              <td>

              </td>
              <td>
                {{$module->limit}}
              </td>
              <td>


                <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
                <!-- Modal -->
              <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Confirmation </h4>
                    </div>
                    <div class="modal-body">

                    <p>  Are you sure you want to delete <b> " {{$module->name}}  "  </b> ?</p>

                          </div>
                    <div class="modal-footer">
                      {!! Form::open(['action' => ['ModuleController@destroy', $module->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                      {{ csrf_field()  }}
                      <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                      {{Form::close() }}
                      <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
                  </div>

                </div>
              </div>



              </td>
            </tr>
            @endforeach

          </tbody>
        </table>



      @endif



@endsection
