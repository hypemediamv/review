@extends('layouts.dashboard')
@section('content')


@include('partials.alerts')

<h2> Change User Settings </h2>
{!! Form::open(['action' => ['UserController@update_user'],  'method' => 'post', 'name' => 'editor_form']) !!}

{!! Form::select('item_id', $users->pluck('name','id') ,null, ['class' => 'form-control select2']) !!}




@if($users != '[]')

<h2> Users</h2>

<table class="table table-hover " >
    <thead>
      <tr>
        <th>Username </th>
        <th>User Group </th>
        <th> Is Expert </th>
        <th>Created At</th>

        <th class="col-sm-1">Current Points </th>
        <th class="col-sm-1">Used Points</th>
        <th class="col-sm-1">All Points</th>


      </tr>
    </thead>
    <tbody>

      @foreach($users as $key => $user)
      <tr>
        <td>
          <strong>{{$user->name}} </strong>
        </td>

        <td>
          {{$user->user_group->name}}
        </td>

        <td>
          @if($user->is_expert!=0)
           Not an Expert
          @else
          An Expert Reviewer

          @endif
        </td>
        <td>
          10
        </td>
        <td>
          20
        </td>
        <td>
          30
        </td>
        <td>
          30
        </td>


      </tr>
      @endforeach

    </tbody>
  </table>


@endif

@endsection
