<h1> Edit Blog Post </h1>
{!! Form::open(['action' => ['BlogPostController@update',Session::get('edit_post')->id ],  'method' => 'patch', 'class' => 'form-group']) !!}
<div class="form-group">
<label for "rank"> Blog Post Title</label>
<input type="text"name="name" class="form-control" value="{{Session::get('edit_post')->name}}">
</div>

<div class="form-group">
  <label for="text">Blog Category</label>
  {!! Form::select('blog_category_id', $blog_categories->pluck('name','id') ,Session::get('edit_post')->blog_category_id, ['class' => 'form-control select2']) !!}
</div>


<div class="form-group">
  <label for="text">Content:</label>

  <textarea id="froala-editor-full" name="content" required> {{ Session::get('edit_post')->content}}</textarea>

</div>

{!! csrf_field() !!}
<button type="submit" class="btn btn-success"> Update </button>
{!! Form::close() !!}
