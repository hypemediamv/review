<h1> Add Blog Post </h1>
{!! Form::open(['action' => ['BlogPostController@store' ],  'method' => 'post', 'class' => 'form-group']) !!}
<div class="form-group">
<label for "rank"> Blog Post Title</label>
<input type="text"name="name" class="form-control">
</div>

<div class="form-group">
  <label for="text">Blog Category</label>
  {!! Form::select('blog_category_id', $blog_categories->pluck('name','id') ,null, ['class' => 'form-control select2']) !!}
</div>


<div class="form-group">
  <label for="text">Content:</label>

  <textarea id="froala-editor-full" name="content" required> {{old('content')}}</textarea>

</div>

{!! csrf_field() !!}
<button type="submit" class="btn btn-success"> Add </button>
{!! Form::close() !!}
