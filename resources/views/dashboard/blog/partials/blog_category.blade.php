
    <h1> Add Blog Category </h1>
    {!! Form::open(['action' => ['BlogCategoryController@store' ],  'method' => 'post']) !!}
    <div class="form-group">
    <label for "rank"> Blog Category Name  </label>
    <input type="text"name="name" >
    </div>
    {!! csrf_field() !!}
    <button type="submit" class="btn btn-success"> Add </button>
    {!! Form::close() !!}


    @if($blog_categories!= null)
    <h2>Blog Categories </h2>
    <table class="table table-hover" >
        <thead>
          <tr>
            <th >Category Name </th>

            <th>Functions</th>
          </tr>
        </thead>
        <tbody>

          @foreach($blog_categories as $key => $blog_category)
          <tr>

            <td> {{$blog_category->name }}</td>


            <td>
              {!! Form::open(['action' => ['BlogCategoryController@update',$blog_category->id ],  'method' => 'patch', 'style' => 'display:inline-block;']) !!}
              <input type="text" value="{{$blog_category->name}}" name="name">
              <button type="submit" class="btn btn-success"> Update </button>

              {!! Form::close() !!}

              <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
              <!-- Modal -->
            <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Confirmation </h4>
                  </div>
                  <div class="modal-body">

                  <p>  Are you sure you want to delete <b> " {{$blog_category->name}}  "  </b> ?</p>

                        </div>
                  <div class="modal-footer">
                    {!! Form::open(['action' => ['BlogCategoryController@destroy', $blog_category->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                    {{ csrf_field()  }}
                    <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                    {{Form::close() }}
                    <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
                </div>

              </div>
            </div>



            </td>
          </tr>
          @endforeach

        </tbody>
      </table>



    @endif
