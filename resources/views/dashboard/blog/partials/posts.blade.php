
@if(Session::get('view_post') != null)
<div class="well">
  <a href="{{ action('BlogController@index')}}"<button class="btn btn-default pull-right" type="button"> Close </button></a>
  <h4>Title:</h4>

<h2>{{ Session::get('view_post')->name }} </h2>

<hr>

<h4>Content:</h4>
{!!  Session::get('view_post')->content !!}
</div>
@endif

@if(Session::get('edit_post')!= null)
@include('dashboard.blog.partials.edit_post')
@endif



    @if($blog_posts!= null)
    <h2>Blog Posts </h2>
    <table class="table table-hover" id="table_id">
        <thead>
          <tr>
            <th >Post </th>

            <th>Functions</th>
          </tr>
        </thead>
        <tbody>

          @foreach($blog_posts as $key => $blog_post)
          <tr>

            <td>{{$blog_post->name }} </a></td>


            <td>
<a href=" {{action('BlogPostController@show', $blog_post->id) }}"> <button type="button" class="btn btn-default"> View </button></a>
<a href=" {{action('BlogPostController@edit', $blog_post->id) }}"> <button type="button" class="btn btn-default"> Edit </button></a>

              <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-blog-post-modal-{{$key}}">Delete</button>
              <!-- Modal -->
            <div id="delete-blog-post-modal-{{$key}}" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Confirmation </h4>
                  </div>
                  <div class="modal-body">

                  <p>  Are you sure you want to delete <b> " {{$blog_post->name}}  "  </b> ?</p>

                        </div>
                  <div class="modal-footer">
                    {!! Form::open(['action' => ['BlogPostController@destroy', $blog_post->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                    {{ csrf_field()  }}
                    <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                    {{Form::close() }}
                    <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
                </div>

              </div>
            </div>



            </td>
          </tr>
          @endforeach

        </tbody>
      </table>

      @endif
