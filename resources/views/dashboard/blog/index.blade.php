@extends('layouts.dashboard')
@section('content')

@include('partials.alerts')

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#blog_posts"> Posts</a></li>
  <li><a data-toggle="tab" href="#add_post">Add Post</a></li>
  <li><a data-toggle="tab" href="#blog_categories"> Blog Categories</a></li>
</ul>

<div class="tab-content">




  <div id="blog_categories" class="tab-pane fade">

    @include('dashboard.blog.partials.blog_category')

  </div>

  <div id="add_post" class="tab-pane fade">
    @include('dashboard.blog.partials.add_post')

  </div>


  <div id="blog_posts" class="tab-pane fade in active">
    @include('dashboard.blog.partials.posts')
  </div>
</div>





@endsection
