@extends('layouts.dashboard')
@section('content')


@include('partials.alerts')

<h1> Add Custom Rating  </h1>


{!! Form::open(['action' => ['CustomRatingController@store'],  'method' => 'post' ]) !!}

<div class="form-group">
  <label for="text">Custom  Rate Name </label>
  <input type="text" name="name" class="form-control" required >
</div>

<button type="submit" class="btn btn-success">Add </button>

{!! Form::close() !!}



@if($custom_ratings != '[]')

<h2>Existing Custom Ratings</h2>



<table class="table table-hover " id="table_id">
    <thead>
      <tr>
        <th>Custom  Rate Name </th>
        <th class="col-sm-1">
          Created By
        </th>
        <th class="col-sm-1">Functions</th>


      </tr>
    </thead>
    <tbody>

      @foreach($custom_ratings as $key => $custom_rating)
      <tr>



        <td>{{ $custom_rating->name }} </td>
        <td>{{ $custom_rating->created_at->diffForHumans()}} </td>

        <td>


          <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
          <!-- Modal -->
        <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation </h4>
              </div>
              <div class="modal-body">

              <p>  Are you sure you want to delete <b> " {{$custom_rating->name}}  "  </b> ?</p>

                    </div>
              <div class="modal-footer">
                {!! Form::open(['action' => ['CustomRatingController@destroy', $custom_rating->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                {{ csrf_field()  }}
                <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                {{Form::close() }}
                <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
            </div>

          </div>
        </div>



        </td>


      </tr>
      @endforeach

    </tbody>
  </table>

@endif

@endsection
