@extends('layouts.dashboard')

@section('content')
    @include('partials.alerts')
    @if(Session::get('edit_item') != null)
        @include('dashboard.ads.partials.form')
    @else
    <h1> Add Ads </h1>
    {!! Form::open(['action' => ['AdsController@store'],  'method' => 'post', 'files'=> 'true']) !!}
    <div class="form-group">
        <label for="text">Ads Name:</label>
        <input type="text" name="name" class="form-control" id="name" required>
    </div>

    <div class="form-group">
        <label for="image">Image:</label>
        <input type="file" name="image" id="bootstrap-input" required/>
    </div>

    <div class="form-group">
        <label for="text">Ads Owner:</label>
        <input type="text" name="owner" class="form-control" id="owner" required>
    </div>

    <div class="form-group">
        <label for="text">Ads Expiry Date:</label>
        <input type="date" name="expiry_date" class="form-control" id="expiry_date" required>
    </div>
    <br>
    <button type="submit" class="btn btn-default">Submit</button>
    {!! Form::close() !!}
    @endif
    @if($ads != '[]')

        <h2>Existing Ads </h2>

        <table class="table" id="table_id">
            <thead>
            <tr>
                <th>Ads Name </th>
                <th>Ads Owned by</th>
                <th>Ads Expired date</th>
                <th>Actions</th>


            </tr>
            </thead>
            <tbody>

            @foreach($ads as $key => $ad)
                <tr>
                    <td>{{$ad->name}}</td>
                    <td>{{$ad->owner}}</td>
                    <td>{{$ad->expiry_date}}</td>
                    <td>
                        <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#delete-modal-{{$key}}">Delete</button>
                        <!-- Modal -->
                        <div id="delete-modal-{{$key}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Delete Confirmation</h4>
                                    </div>
                                    <div class="modal-body">

                                        <p>  Are you sure you want to delete <b> " {{$ad->name}}  "  </b> ?</p>

                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::open(['action' => ['AdsController@destroy', $ad->id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                                        {{ csrf_field()  }}
                                        <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                                        {{Form::close() }}
                                        <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
                                </div>

                            </div>
                        </div>
                        <a href="{{ action('AdsController@edit', $ad->id ) }}">
                            <button type="button" class="btn btn-default ">Edit</button>
                        </a>


                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

    @endif

@endsection