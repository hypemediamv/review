<h4>
@if($status->name == 'draft')
  <span class="label label-default">
@endif

@if($status->name == 'pending')
  <span class="label label-warning">
@endif

@if($status->name == 'approved')
  <span class="label label-success">
@endif

@if($status->name == 'rejected')
  <span class="label label-danger">
@endif

{{ $status->name}}   </span></h4>
