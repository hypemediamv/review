<div id="delete-{{$modelName}}-{{$key}}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation </h4>
            </div>
            <div class="modal-body">

                <p>  Are you sure you want to delete <b> " {{$name}}  "  </b> ?</p>

            </div>
            <div class="modal-footer">
                {!! Form::open(['action' => ["$controller@destroy", $id ],  'method' => 'delete' ,'style' => 'display:inline-block']) !!}
                {{ csrf_field()  }}
                <button type="submit" class="btn btn-danger"  style="display:inline-block;" >Yes, Delete</button>
                {{Form::close() }}
                <button type="button" class="btn btn-default" style="display:inline-block;" data-dismiss="modal">No</button>      </div>
        </div>

    </div>
</div>