$(function() {
      $('[data-toggle="tooltip"]').tooltip();
      $('.select2').select2();
      $('textarea#froala-editor').froalaEditor({

        toolbarButtons: ['bold',  'italic', 'underline','fontSize',  'formatOL', 'formatUL', 'indent', 'outdent'],
        heightMin: 200,
        heightMax: 200,
        charCounterMax: 1000,
        charCounterCount: true

      });

      $('textarea#froala-editor-full').froalaEditor({

        toolbarButtons: ['bold',  'italic', 'underline','fontSize',  'formatOL', 'formatUL', 'indent', 'outdent'],
        heightMin: 400,
        heightMax: 400,
        charCounterMax: 5000,

      })

      $('#product_view_image').ezPlus({
          gallery: 'product_view_gallery', cursor: 'pointer', galleryActiveClass: 'active',
          imageCrossfade: true,
      });

      //pass the images to Fancybox
      $('#product_view_image').bind('click', function (e) {
          var ez = $('#product_view_gallery').data('ezPlus');
          $.fancyboxPlus(ez.getGalleryList());
          return false;
      });




});
